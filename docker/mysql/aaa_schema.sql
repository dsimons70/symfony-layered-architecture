create table User
(
    user_id    int auto_increment primary key,
    identifier char(36)     null,
    username   varchar(128) not null,
    passwd     varchar(128)  null,
    constraint User_identifier_uindex unique (identifier),
    constraint User_username_uindex unique (username)
);

create table ToDos
(
    todo_id         int auto_increment primary key,
    identifier      char(36) not null,
    user_identifier char(36) not null,
    schedule_date   datetime not null,
    constraint ToDos_identifier_unique unique (identifier),
    constraint ToDos_User_identifier_fk foreign key (user_identifier) references User (identifier) on delete cascade
);

create table Tasks
(
    task_id       int auto_increment
        primary key,
    identifier    char(36)     not null,
    todo_id       int          not null,
    schedule_time time         not null,
    description   varchar(256) not null,
    is_done       tinyint(1)   not null,
    constraint Tasks_identifier_unique unique (identifier),
    constraint Tasks_ToDos_todo_id_fk foreign key (todo_id) references ToDos (todo_id) on delete cascade
);

create index Tasks_schedule_time_index
    on Tasks (schedule_time);

create index ToDos_user_identifier_index
    on ToDos (user_identifier);

