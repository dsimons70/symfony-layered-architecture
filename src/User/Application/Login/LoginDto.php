<?php

declare(strict_types=1);

namespace App\User\Application\Login;

class LoginDto
{
    private string $username;
    private string $password;
    private string $repoTypeIdentifier;

    public function __construct(string $username, string $password, string $repoTypeIdentifier)
    {
        $this->username = trim($username);
        $this->password = trim($password);
        $this->repoTypeIdentifier = trim($repoTypeIdentifier);
    }

    /**
     * Return the identifier of the selected repository type
     */
    public function getPersistenceTypeIdentifier(): string
    {
        return $this->repoTypeIdentifier;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
