<?php

declare(strict_types=1);

namespace App\User\Application\Login;

use App\Shared\Infrastructure\Persistence\PersistenceFactory;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\User\Domain\Model\User;
use App\User\Domain\Model\UserRepository;
use App\User\Domain\Service\UserCredentialService;
use App\User\Infrastructure\Session\SessionRegistry;
use RuntimeException;

class UserLoginService
{
    private SessionRegistry $sessionRegistry;
    private PersistenceFactory $persistenceFactory;
    private UserRepository $userRepository;
    private UserCredentialService $userCredentialsService;

    public function __construct(
        SessionRegistry $sessionRegistry,
        PersistenceFactory $repositoryFactory,
        UserRepository $userRepository,
        UserCredentialService $userCredentialsService
    ) {
        $this->sessionRegistry        = $sessionRegistry;
        $this->persistenceFactory     = $repositoryFactory;
        $this->userRepository         = $userRepository;
        $this->userCredentialsService = $userCredentialsService;
    }

    /**
     * User Login and session init
     * @throws RuntimeException
     */
    public function checkLogin(LoginDto $loginDto): ?User
    {
        $user           = $this->getUser($loginDto->getUsername());
        $typeIdentifier = $loginDto->getPersistenceTypeIdentifier();

        if (!$user instanceof User) {
            return null;
        }

        $hasValidCredentials = $this->userCredentialsService->isValid(
            $user->getCredentials(),
            $loginDto->getUsername(),
            $loginDto->getPassword()
        );

        if (!$hasValidCredentials) {
            return null;
        }

        if (!$this->persistenceFactory->isPersistenceTypRegistered($typeIdentifier)) {
            throw new RuntimeException('Repository type is invalid');
        }

        $this->sessionRegistry->setPersistenceTypeIdentifier($typeIdentifier);
        $this->sessionRegistry->setUser($user);

        return $user;
    }

    /**
     * Returns the user or null
     */
    public function getUser(string $username): ?User
    {
        return $this->userRepository->findByUsername($username);
    }

    /**
     * Return all registered persistence type
     * @return PersistenceType[]
     */
    public function getRegisteredPersistenceTypes(): array
    {
        return $this->persistenceFactory->getAllPersistenceTypes();
    }
}
