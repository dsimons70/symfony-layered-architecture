<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Session;

use App\User\Domain\Model\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Symfony based session registry implementation
 */
class SymfonySessionRegistry implements SessionRegistry
{
    private const PREFIX = 'registry.';
    private SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @inheritDoc
     */
    public function setUser(User $user): void
    {
        $this->session->set(self::PREFIX . 'user', $user);
    }

    /**
     * @inheritDoc
     */
    public function getUser(): ?User
    {
        return $this->session->get(self::PREFIX . 'user');
    }

    /**
     * @inheritDoc
     */
    public function setPersistenceTypeIdentifier(string $repositoryType): void
    {
        $this->session->set(self::PREFIX . 'persistence_type_identifier', $repositoryType);
    }

    /**
     * @inheritDoc
     */
    public function getPersistenceTypeIdentifier(): ?string
    {
        return $this->session->get(self::PREFIX . 'persistence_type_identifier');
    }
}
