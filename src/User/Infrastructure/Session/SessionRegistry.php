<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Session;

use App\User\Domain\Model\User;

/**
 * Session interface for our app, to decouple session handling from framework specific code
 */
interface SessionRegistry
{
    /**
     * Set the current user domain model
     */
    public function setUser(User $user): void;

    /**
     * Return the current user domain model
     */
    public function getUser(): ?User;

    /**
     * Set the identifier of the selected persistence technology
     */
    public function setPersistenceTypeIdentifier(string $repositoryType): void;

    /**
     * Get the identifier for the selected persistence technology
     * @return string|null
     */
    public function getPersistenceTypeIdentifier(): ?string;
}
