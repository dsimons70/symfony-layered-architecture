<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Authentication;

use App\User\Application\Login\LoginDto;
use App\User\Application\Login\UserLoginService;
use App\User\Domain\Model\User;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthUserFormGuardAuthenticator extends AbstractGuardAuthenticator
{
    /** @var RouterInterface */
    private $router;

    /** @var ValidatorInterface */
    private $validator;

    /** @var UserLoginService */
    private $userLoginService;

    public function __construct(
        RouterInterface $router,
        UserLoginService $userLoginService,
        ValidatorInterface $validator
    ) {
        $this->router           = $router;
        $this->userLoginService = $userLoginService;
        $this->validator        = $validator;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    protected function getLoginUrl(): string
    {
        return $this->router->generate('login');
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->getLoginUrl());
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): bool
    {
        if ($request->get('_route') === 'login' && $request->get('formAction') === 'loginUser') {
            return true;
        };

        return false;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getCredentials(Request $request): LoginDto
    {
        $loginDto = new LoginDto(
            $request->get('username', ''),
            $request->get('password', ''),
            $request->get('persistenceType', ''),
        );

        $errors = $this->validator->validate($loginDto);

        if (count($errors) > 0) {
            throw new AuthenticationException($errors->get(0)->getMessage());
        }

        return $loginDto;
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        /** @var LoginDto $loginDto */
        $loginDto   = $credentials;
        $domainUser = $this->userLoginService->getUser($loginDto->getUsername());

        if (!$domainUser instanceof User) {
            throw new AuthenticationException('Invalid username or password');
        }

        return AuthUserDecorator::fromDomainUser($domainUser);

    }

    /**
     * @inheritDoc
     * @throws RuntimeException
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        /** @var LoginDto $loginDto */
        $loginDto = $credentials;

        if ($this->userLoginService->checkLogin($loginDto) === null) {
            throw new AuthenticationException('Invalid username or password');
        };

        return true;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->request->set('errorMessage', $exception->getMessage());
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return new RedirectResponse($this->router->generate('list_todos'));
    }

    /**
     * @inheritDoc
     */
    public function supportsRememberMe()
    {
        false;
    }
}
