<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Authentication;

use App\User\Application\Login\UserLoginService;
use App\User\Domain\Model\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AuthUserProvider implements UserProviderInterface
{
    private UserLoginService $userLoginService;

    public function __construct(UserLoginService $userLoginService)
    {
        $this->userLoginService = $userLoginService;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername($username): UserInterface
    {
        $user = $this->userLoginService->getUser($username);

        if (!$user instanceof User) {
            throw new UsernameNotFoundException();
        }

        return AuthUserDecorator::fromDomainUser($user);
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        $authUser   = $user;
        $domainUser = $this->userLoginService->getUser($authUser->getUsername());

        if (!$authUser instanceof AuthUserDecorator) {
            throw new UnsupportedUserException('User not supported');
        }

        if (!$domainUser instanceof User) {
            throw new UsernameNotFoundException();
        }

        return AuthUserDecorator::fromDomainUser($domainUser);
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class): bool
    {
        return $class === AuthUserDecorator::class;
    }
}
