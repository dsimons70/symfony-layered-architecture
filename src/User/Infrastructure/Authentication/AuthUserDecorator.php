<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Authentication;

use App\User\Domain\Model\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * We want to keep our core domain (application- + domain-layer) free from framework code, that's why we
 * create this decorator as an adapter to our external (infrastructure) symfony authentication interfaces.
 */
class AuthUserDecorator implements UserInterface
{
    private User $decoratedUser;

    private function __construct(User $user)
    {
        $this->decoratedUser = $user;
    }

    /**
     * Named constructor to create an instance based on the 'real' user model
     * from the domain layer
     */
    public static function fromDomainUser(User $user)
    {
        return new self($user);
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->decoratedUser->getRoles()->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->decoratedUser->getCredentials()->getPassword();
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->decoratedUser->getCredentials()->getUsername();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // void
    }
}
