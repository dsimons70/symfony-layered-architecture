<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Encryption;

use App\User\Domain\Service\PasswordHashing;
use Symfony\Component\Security\Core\Encoder\SodiumPasswordEncoder;

/**
 * Infrastructure implementation of a password hashing service.
 * We used the build in symfony encoder
 */
class SodiumPasswordHashingService implements PasswordHashing
{
    /** @var SodiumPasswordEncoder */
    private $sodiumEncoder;

    public function __construct()
    {
        $this->sodiumEncoder = new SodiumPasswordEncoder();
    }

    /**
     * @inheritDoc
     */
    public function hashPassword(string $password): string
    {
        return $this->sodiumEncoder->encodePassword($password, null);
    }

    /**
     * @inheritDoc
     */
    public function verifyPassword(string $hashedPassword, string $rawPassword): bool
    {
        return $this->sodiumEncoder->isPasswordValid($hashedPassword, $rawPassword, null);
    }
}
