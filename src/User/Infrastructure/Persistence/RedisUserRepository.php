<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\PersistenceIdentification;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\Shared\Infrastructure\Persistence\Redis\RedisConnection;
use App\Shared\Infrastructure\Persistence\Redis\RedisPersistenceType;
use App\User\Domain\Model\User;
use App\User\Domain\Model\UserCredentials;
use App\User\Domain\Model\UserIdentifier;
use App\User\Domain\Model\UserRepository;
use Redis;
use RuntimeException;

/**
 * UserRepository implementation for redis databases
 *
 * We following the 'Interface segregation principle' from SOLID to keep our
 * domain layer repositories clean from infrastructure dependencies
 *
 * Redis layout:
 *
 * users:1              => data hash map (key => hash map)
 * users:names:john.doe => user:1        (key => value)
 * users:identifier     => last user id  (INCR)
 *
 */
class RedisUserRepository implements UserRepository, PersistenceIdentification
{
    private Redis $redis;

    /**
     * @throws RuntimeException
     */
    public function __construct(RedisConnection $redisConnection)
    {
        $this->redis = $redisConnection->connect();
    }

    /**
     * @inheritDoc
     */
    public function addUser(User $user): void
    {
        $usernameKey = $this->generateUsernameKey($user->getCredentials()->getUsername());
        $userKey     = $user->getIdentifier();

        $userData = [
            "username" => $user->getCredentials()->getUsername(),
            "password" => $user->getCredentials()->getPassword(),
        ];

        $this->redis->hMSet($userKey, $userData); // user:1 => data
        $this->redis->set($usernameKey, $userKey); // user:name:john.doe => users:1
    }

    /**
     * @inheritDoc
     */
    public function findByUsername(string $username): ?User
    {
        $usernameKey = $this->generateUsernameKey($username);
        $userKey     = $this->redis->get($usernameKey);

        if ($userKey === false) {
            return null;
        }

        $data = $this->redis->hMGet($userKey, ["password", "username"]);

        return User::asStandardUser(
            UserIdentifier::fromString($userKey),
            UserCredentials::fromEncodedPassword($data['password'], $data['username'])
        );
    }

    /**
     * @inheritDoc
     */
    public function nextUserIdentifier(): UserIdentifier
    {
        $id         = $this->redis->incr('users:identifiers');
        $identifier = 'users:' . $id;

        return UserIdentifier::fromString($identifier);
    }

    /**
     * @inheritDoc
     */
    public function getPersistenceType(): PersistenceType
    {
        return new RedisPersistenceType();
    }

    /**
     * @inheritDoc
     */
    public function getImplementedRepositoryInterface(): string
    {
        return UserRepository::class;
    }

    /**
     * Generate the key for the username to user key mapping
     */
    private function generateUsernameKey(string $username): string
    {
        return "users:names:" . $username;
    }
}
