<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\MySql\MySqlConnection;
use App\Shared\Infrastructure\Persistence\MySql\MySqlPersistenceType;
use App\Shared\Infrastructure\Persistence\PersistenceIdentification;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\User\Domain\Model\User;
use App\User\Domain\Model\UserCredentials;
use App\User\Domain\Model\UserIdentifier;
use App\User\Domain\Model\UserRepository;
use Exception;
use PDO;
use PDOException;
use RuntimeException;

/**
 * UserRepository implementation for MySql databases
 *
 * We following the 'Interface segregation principle' from SOLID to keep our
 * domain layer repositories clean from infrastructure dependencies
 */
class MySqlUserRepository implements UserRepository, PersistenceIdentification
{
    private PDO $pdo;

    /**
     * @throws Exception
     */
    public function __construct(MySqlConnection $mySqlConnectionFactory)
    {
        $this->pdo = $mySqlConnectionFactory->connect();
    }

    /**
     * @inheritDoc
     * @throws PDOException
     * @throws RuntimeException
     */
    public function addUser(User $user): void
    {
        $this->pdo->beginTransaction();

        try {
            $stmd = $this->pdo->prepare(
                <<<SQL
                INSERT INTO todo.User (identifier, username, passwd) 
                VALUES (:identifier, :username, :password)
                SQL
            );

            $stmd->bindValue('identifier', $user->getIdentifier(), PDO::PARAM_STR);
            $stmd->bindValue('username', $user->getCredentials()->getUsername(), PDO::PARAM_STR);
            $stmd->bindValue('password', $user->getCredentials()->getPassword(), PDO::PARAM_STR);
            $stmd->execute();
        } catch (PDOException $exception) {
            $this->pdo->rollBack();
            throw new RuntimeException("Can't insert user");
        }

        $this->pdo->commit();
    }

    /**
     * @inheritDoc
     * @throws PDOException
     */
    public function findByUsername(string $username): ?User
    {
        $stmd = $this->pdo->prepare(
            <<<SQL
            SELECT identifier, username, passwd 
            FROM todo.User 
            WHERE username = :username
            SQL
        );

        $stmd->bindValue('username', $username, PDO::PARAM_STR);
        $stmd->execute();

        $row = $stmd->fetch(PDO::FETCH_ASSOC);

        if (!is_array($row)) {
            return null;
        }

        return $this->createUser($row);
    }

    /**
     * @inheritDoc
     */
    public function getPersistenceType(): PersistenceType
    {
        return new MySqlPersistenceType();
    }

    /**
     * @inheritDoc
     */
    public function getImplementedRepositoryInterface(): string
    {
        return UserRepository::class;
    }

    /**
     * Create entity from array
     */
    private function createUser(array $row): User
    {
        return User::asStandardUser(
            UserIdentifier::fromString($row['identifier']),
            UserCredentials::fromEncodedPassword($row['passwd'], $row['username'])
        );
    }
}
