<?php

declare(strict_types=1);

namespace App\User\Presentation\Controller;

use App\User\Application\Login\UserLoginService;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{
    private UserLoginService $userLoginService;

    public function __construct(UserLoginService $userLoginService)
    {
        $this->userLoginService = $userLoginService;
    }

    /**
     * Application services are for controllers, but in order to plugin into the symfony authentication
     * the most calls of our UserLoginService are hidden in the GuardAuthenticator. We want to keep our
     * core domain (application- + domain-layer) free from framework code, so we use our application
     * service in the GuardAuthenticator
     *
     * @throws LogicException
     * @see \App\User\Infrastructure\Authentication\AuthUserProvider
     *
     * @Route("/", name="login")
     * @see \App\User\Infrastructure\Authentication\AuthUserFormGuardAuthenticator
     */
    public function login(): Response
    {
        $params = [
            'persistenceTypes' => $this->userLoginService->getRegisteredPersistenceTypes(),
        ];

        return $this->render('@user/login.html.twig', $params);
    }
}
