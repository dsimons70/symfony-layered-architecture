<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

use App\Shared\Domain\Model\Identifier;

class UserIdentifier extends Identifier
{
    public static function fromString(string $identifier): self
    {
        return new static($identifier);
    }
}
