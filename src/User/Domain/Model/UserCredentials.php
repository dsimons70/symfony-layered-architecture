<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

class UserCredentials
{
    private string $username;
    private string $password;

    private function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public static function fromEncodedPassword(string $password, string $username): UserCredentials
    {
        return new self($username, $password);
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
