<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

interface UserRepository
{
    /**
     * Add a user
     */
    public function addUser(User $user): void;

    /**
     * find a user by name
     */
    public function findByUsername(string $username): ?User;
}
