<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

class User
{
    private UserIdentifier $identifier;
    private UserRoles $roles;
    private UserCredentials $credentials;

    private function __construct(UserIdentifier $identifier, UserCredentials $userCredentials, UserRoles $userRoles)
    {
        $this->identifier  = $identifier;
        $this->credentials = $userCredentials;
        $this->roles       = $userRoles;
    }

    /**
     * Named constructor to create an instance with standard roles
     */
    public static function asStandardUser(UserIdentifier $identifier, UserCredentials $userCredentials): User
    {
        return new self(
            $identifier,
            $userCredentials,
            UserRoles::forStandardUser()
        );
    }

    /**
     * @return UserCredentials
     */
    public function getCredentials(): UserCredentials
    {
        return $this->credentials;
    }

    /**
     * Returns the user roles
     */
    public function getRoles(): UserRoles
    {
        return $this->roles;
    }

    /**
     * Returns the user identifier
     */
    public function getIdentifier(): UserIdentifier
    {
        return $this->identifier;
    }
}
