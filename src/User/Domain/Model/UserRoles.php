<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

use ArrayIterator;
use IteratorAggregate;

class UserRoles implements IteratorAggregate
{
    /** @var string */
    public const STANDARD_USER = 'ROLE_STANDARD_USER';

    /** @var string[] */
    private array $roles;

    private function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Named constructor to create an instance with standard roles
     */
    public static function forStandardUser(): UserRoles
    {
        return new self([self::STANDARD_USER]);
    }

    /**
     * Converts the object to array
     */
    public function toArray(): array
    {
        return $this->roles;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->roles);
    }
}
