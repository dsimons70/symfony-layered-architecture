<?php

declare(strict_types=1);

namespace App\User\Domain\Service;

use App\User\Domain\Exception\UserRuntimeException;
use App\User\Domain\Model\UserCredentials;
use InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class UserCredentialService
{
    private PasswordHashing $passwordHashing;

    public function __construct(PasswordHashing $passwordHashingService)
    {
        $this->passwordHashing = $passwordHashingService;
    }

    /**
     * Creates the user credentials object with a hashed password token
     * @throws UserRuntimeException
     */
    public function encode(string $username, string $password): UserCredentials
    {
        try {
            $encoded = $this->passwordHashing->hashPassword($password);
        } catch (InvalidArgumentException | BadCredentialsException $exception) {
            throw UserRuntimeException::forInvalidPasswordEncryption($password, $exception);
        }

        return UserCredentials::fromEncodedPassword($encoded, $username);
    }

    /**
     * Encrypts the user credentials and compares them with the given raw values
     */
    public function isValid(UserCredentials $credentials, string $rawUsername, string $rawPassword): bool
    {
        try {
            $isValid = $this->passwordHashing->verifyPassword($credentials->getPassword(), $rawPassword);
        } catch (InvalidArgumentException $exception) {
            return false;
        }

        if (!$isValid) {
            return false;
        }

        return $credentials->getUsername() === $rawUsername;
    }
}
