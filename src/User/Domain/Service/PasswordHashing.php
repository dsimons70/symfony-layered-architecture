<?php

declare(strict_types=1);

namespace App\User\Domain\Service;

use InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Interface for the infrastructure layer to implement complex password hashing and verification
 */
interface PasswordHashing
{
    /**
     * Encrypts the given password
     * @throws InvalidArgumentException
     * @throws BadCredentialsException
     */
    public function hashPassword(string $password): string;

    /**
     * Decrypts the hashed password and compares it with the given raw value
     * @throws InvalidArgumentException
     */
    public function verifyPassword(string $hashedPassword, string $rawPassword): bool;
}
