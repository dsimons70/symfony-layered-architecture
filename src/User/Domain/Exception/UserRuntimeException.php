<?php

declare(strict_types=1);

namespace App\User\Domain\Exception;

use RuntimeException;

class UserRuntimeException extends RuntimeException
{
    private string $password;

    /**
     * Named constructor for password encryption exceptions
     */
    public static function forInvalidPasswordEncryption(string $password, $previous): UserRuntimeException
    {
        $instance           = new static('Error during password encryption', 0, $previous);
        $instance->password = $password;

        return $instance;
    }
}
