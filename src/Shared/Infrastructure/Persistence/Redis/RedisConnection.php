<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Redis;

use Redis;
use RuntimeException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RedisConnection
{
    private string $host;
    private string $namespace;
    private ?Redis $redis;

    /**
     * Allow autowire for services with config parameters
     * @see https://symfony.com/blog/new-in-symfony-4-1-getting-container-parameters-as-a-service
     * @throws ParameterNotFoundException
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->host      = (string) $parameterBag->get('app.redis.host');
        $this->namespace = (string) $parameterBag->get('app.redis.namespace');
        $this->redis = null;
    }

    /**
     * @throws RuntimeException
     */
    public function connect(): Redis
    {
        if (!$this->redis instanceof Redis) {
            $this->redis = $this->doConnect();
        }

        return $this->redis;
    }

    /**
     * Connect and set default options
     * @throws RuntimeException
     */
    private function doConnect(): Redis
    {
        $redis = new Redis();

        if (!$redis->connect($this->host)) {
            throw new RuntimeException("Can't connect to redis");
        };

        $redis->setOption(Redis::OPT_PREFIX, $this->namespace);

        return $redis;
    }
}
