<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Redis;

use App\Shared\Infrastructure\Persistence\PersistenceType;

class RedisPersistenceType implements PersistenceType
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return "Redis";
    }

    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return 'type_redis';
    }
}
