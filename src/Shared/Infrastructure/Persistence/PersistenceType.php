<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence;

/**
 * Interface for our PersistenceType's like Redis, MySql, ...
 *
 * We following the 'Dependency inversion principle' to keep the PersistenceFactory clean
 */
interface PersistenceType
{
    /**
     * Returns the display name
     */
    public function getName(): string;

    /**
     * Returns the identifier string
     */
    public function getIdentifier(): string;
}
