<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\Config\PersistenceConfig;
use Exception;
use RuntimeException;

/**
 * Factory to create/select repositories based on the requested repository interface from the domain
 * layer and the current configured PersistenceType at runtime
 */
class PersistenceFactory
{
    private PersistenceConfig $persistenceConfig;

    /** @var PersistenceIdentification[] */
    private array $repositories;

    /** @var PersistenceType[] */
    private array $persistenceTypes;

    /**
     * We are using the PersistenceConfig interface to follow the 'Open/closed principle',
     * 'Single responsibility principle' and the 'Dependency inversion principle' from SOLID
     *
     * @throws Exception
     */
    public function __construct(iterable $repositories, PersistenceConfig $configuration)
    {
        $this->persistenceConfig = $configuration;

        foreach ($repositories as $repository) {
            $this->addRepository($repository);
        }
    }

    /**
     * Returns the repository which implements the given repository interface based on
     * the current configured persistence type
     *
     * @throws Exception
     */
    public function getRepository(string $repositoryInterface): PersistenceIdentification
    {
        $configuredType  = $this->persistenceConfig->getConfiguredType();
        $persistenceType = $this->getPersistenceType($configuredType);

        $persistenceIdentifier = $persistenceType->getIdentifier();
        $persistenceName       = $persistenceType->getName();

        if (!isset($this->repositories[$persistenceIdentifier][$repositoryInterface])) {
            throw new Exception("Repository interface '$repositoryInterface' is not registered for type '$persistenceName'");
        }

        return $this->repositories[$persistenceIdentifier][$repositoryInterface];
    }

    /**
     * Returns all registered repository types
     * @return PersistenceType[]
     */
    public function getAllPersistenceTypes(): array
    {
        return $this->persistenceTypes;
    }

    /**
     * Checks if the given type is registered and valid
     */
    public function isPersistenceTypRegistered(string $typeIdentifier): bool
    {
        return isset($this->persistenceTypes[$typeIdentifier]);
    }

    /**
     * Return the repository type for the given identifier
     * @throws RuntimeException
     */
    private function getPersistenceType(string $typeIdentifier): PersistenceType
    {
        if (!isset($this->persistenceTypes[$typeIdentifier])) {
            throw new RuntimeException('Repository type not registered');
        }

        return $this->persistenceTypes[$typeIdentifier];
    }

    /**
     * Add the repository and register all uses persistence types
     * @throws Exception
     */
    private function addRepository(PersistenceIdentification $repository): void
    {
        $interface = $this->getImplementedRepositoryInterface($repository);

        $repositoryType = $repository->getPersistenceType();
        $typeIdentifier = $repositoryType->getIdentifier();
        $typeName       = $repositoryType->getName();

        if (!isset($this->persistenceTypes[$typeIdentifier])) {
            $this->persistenceTypes[$typeIdentifier] = $repositoryType;
        }

        if (isset($this->repositories[$typeIdentifier][$interface])) {
            throw new Exception("Repository interface: '$interface' is already registered for type: '$typeName'");
        }

        $this->repositories[$typeIdentifier][$interface] = $repository;
    }

    /**
     * Returns the implemented repository interface
     * @throws RuntimeException
     */
    private function getImplementedRepositoryInterface(PersistenceIdentification $repository): string
    {
        $interface = $repository->getImplementedRepositoryInterface();

        // Ensure that the returned interface has been implemented by the repository class
        if (!in_array($interface, class_implements($repository))) {
            throw new RuntimeException('Implementing repository interface mismatch');
        }

        return $interface;
    }
}
