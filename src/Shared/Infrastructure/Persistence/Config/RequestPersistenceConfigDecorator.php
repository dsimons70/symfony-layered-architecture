<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Config;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Decorates the session persistence config service and return the persistence type provided
 * from the login form. This service is only active if we are on the login page.
 */
class RequestPersistenceConfigDecorator extends PersistenceConfigDecorator
{
    private RequestStack $requestStack;

    public function __construct(SessionPersistenceConfigDecorator $persistenceConfig, RequestStack $requestStack)
    {
        parent::__construct($persistenceConfig);
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritDoc
     */
    public function getConfiguredType(): ?string
    {
        if ($this->matchesLoginRoute()) {
            $persistenceType = $this->requestStack->getCurrentRequest()->get('persistenceType');

            if ($persistenceType !== null) {
                return $persistenceType;
            }
        }

        return $this->persistenceConfig->getConfiguredType();
    }

    /**
     * Check if we are on the login page
     */
    private function matchesLoginRoute(): bool
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request === null) {
            return false;
        }

        return  $request->get('_route') === 'login';
    }
}
