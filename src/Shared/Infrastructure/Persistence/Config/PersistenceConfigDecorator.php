<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Config;

/**
 * Abstract base class to implement the decorator pattern for the persistence config
 *
 * This allows us the read the persistence config from different sources. As an example we have
 * implemented three config options:
 *
 * 1. If we are on the login page we read the config from the request (login form)
 * 2. On all other pages we read the config from our session registry
 * 3. If the session return null we read the default config from the symfony parameter: app.persistence.default
 */
abstract class PersistenceConfigDecorator implements PersistenceConfig
{
    protected PersistenceConfig $persistenceConfig;

    public function __construct(PersistenceConfig $persistenceConfig)
    {
        $this->persistenceConfig = $persistenceConfig;
    }

    /**
     * @inheritDoc
     */
    abstract public function getConfiguredType(): ?string;
}
