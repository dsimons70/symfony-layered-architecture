<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Config;

use App\User\Infrastructure\Session\SessionRegistry;

class SessionPersistenceConfigDecorator extends PersistenceConfigDecorator
{
    private SessionRegistry $sessionRegistry;

    public function __construct(DefaultPersistenceConfig $persistenceConfig, SessionRegistry $sessionRegistry)
    {
        parent::__construct($persistenceConfig);
        $this->sessionRegistry = $sessionRegistry;
    }

    /**
     * @inheritDoc
     */
    public function getConfiguredType(): ?string
    {
        $typeIdentifier = $this->sessionRegistry->getPersistenceTypeIdentifier();

        if ($typeIdentifier === null) {
            return $this->persistenceConfig->getConfiguredType();
        }

        return $typeIdentifier;
    }
}
