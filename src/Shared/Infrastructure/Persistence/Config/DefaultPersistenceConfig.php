<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Config;

use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DefaultPersistenceConfig implements PersistenceConfig
{
    private ParameterBagInterface $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @inheritDoc
     * @throws ParameterNotFoundException
     */
    public function getConfiguredType(): string
    {
        return $this->parameterBag->get('app.persistence.default');
    }
}
