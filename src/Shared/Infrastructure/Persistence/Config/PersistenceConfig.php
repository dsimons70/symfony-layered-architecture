<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\Config;

/**
 * Interface to provide access to the current configured persistence type.
 *
 * We following the 'Open/closed principle' and 'Single responsibility principle'
 * from SOLID to keep our PersistenceFactory clean
 */
interface PersistenceConfig
{
    /**
     * Returns the configured repository type or null
     */
    public function getConfiguredType(): ?string;
}
