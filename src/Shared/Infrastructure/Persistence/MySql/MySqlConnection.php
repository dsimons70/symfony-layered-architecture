<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\MySql;

use PDO;
use PDOException;
use RuntimeException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MySqlConnection
{
    private string $host;
    private string $db;
    private string $user;
    private string $password;
    private ?PDO $pdo = null;

    /**
     * Allow autowire for services with config parameters
     * @see https://symfony.com/blog/new-in-symfony-4-1-getting-container-parameters-as-a-service
     * @throws ParameterNotFoundException
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->host = $parameterBag->get('app.db.host');
        $this->db = $parameterBag->get('app.db.schema');
        $this->user = $parameterBag->get('app.db.user');
        $this->password = $parameterBag->get('app.db.password');
    }

    /**
     * @throws RuntimeException
     */
    public function connect(): PDO
    {
        if (!$this->pdo instanceof PDO) {
            $this->pdo = $this->doConnect();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }

    /**
     * @throws RuntimeException
     */
    private function doConnect(): PDO
    {
        try {
            $pdo = new PDO($this->getDsnString(), $this->user, $this->password);
        } catch (PDOException $exception) {
            throw new RuntimeException("Can't connect to database", 0, $exception);
        }

        return $pdo;
    }

    /**
     * Compose the PDO DSN string
     */
    private function getDsnString(): string
    {
        return 'mysql:host=' . $this->host . ';dbname=' . $this->db;
    }
}
