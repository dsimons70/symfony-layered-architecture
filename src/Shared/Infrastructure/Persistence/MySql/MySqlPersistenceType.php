<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence\MySql;

use App\Shared\Infrastructure\Persistence\PersistenceType;

class MySqlPersistenceType implements PersistenceType
{
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return "MySql";
    }

    /**
     * @inheritDoc
     */
    public function getIdentifier(): string
    {
        return 'type_mysql';
    }
}
