<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Persistence;

/**
 * Interface for infrastructure repositories implementations to make them identifiable based on
 * their persistence type and implemented repository interface from the domain layer
 *
 * We following the 'Interface segregation principle' from SOLID to keep our
 * domain layer repository interfaces clean from infrastructure dependencies
 */
interface PersistenceIdentification
{
    /**
     * Returns the persistence type for this repository
     */
    public function getPersistenceType(): PersistenceType;

    /**
     * Returns the implemented repository interface
     */
    public function getImplementedRepositoryInterface(): string;
}
