<?php

declare(strict_types=1);

namespace App\Shared\Presentation\ViewHelper;

use App\Shared\Domain\Exception\ClientNotification;
use ArrayIterator;
use IteratorAggregate;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class NotificationsViewHelper implements IteratorAggregate
{
    /** @var string[] */
    private array $notifications = [];

    /**
     * Named constructor to create an instance based on the given ConstraintViolationListInterface
     */
    public static function fromConstraintViolationList(ConstraintViolationListInterface $constraintViolationList): NotificationsViewHelper {
        $instance = new self();

        /** @var ConstraintViolation $constraintViolation */
        foreach ($constraintViolationList as $constraintViolation) {
            $payload = $constraintViolation->getConstraint()->payload;
            $errorMessage = $constraintViolation->getMessage();

            // Step 1: Check if we have to use the alias as an error key
            if (empty($payload['alias'])) {
                $instance->addNotification($constraintViolation->getPropertyPath(), $errorMessage);
                continue;
            }

            // Step 2: Use the alias for the error key
            $errorKey = $payload['alias'];

            // Step 3: Check if we have to parse a unique identifier from the property path
            if (isset($payload['prefix'])) {
                preg_match($payload['prefix'], $constraintViolation->getPropertyPath(), $matches);

                if (!empty($matches['prefix'])) {
                    $errorKey = $matches['prefix'] . '.' . $errorKey;
                }
            }

            $instance->addNotification($errorKey, $errorMessage);
        }

        return $instance;
    }

    /**
     * Named constructor for domain layer exceptions, that implements the client notification
     * interface to provide notifications for the presentation layer
     */
    public static function fromNotificationException(ClientNotification $clientNotification): NotificationsViewHelper
    {
        $instance = new self();

        $instance->addNotification(
            $clientNotification->getOrigin(),
            $clientNotification->getUserNotification()
        );

        return $instance;
    }

    /**
     * Checks if a violation for the given key exists
     */
    public function hasViolation(string $key): bool
    {
        return isset($this->notifications[$key]);
    }

    /**
     * Returns the constraint violation message for the given key
     */
    public function getViolationMessage(string $key): string
    {
        if (!isset($this->notifications[$key])) {
            return '';
        }

        return $this->notifications[$key];
    }

    /**
     * @return string[]
     */
    public function getNotifications(): array
    {
        return $this->notifications;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->notifications);
    }

    /**
     * Build constraint violation array
     */
    private function addNotification(string $errorKey, string $errorMessage): void
    {
        $this->notifications[$errorKey] = $errorMessage;
    }
}
