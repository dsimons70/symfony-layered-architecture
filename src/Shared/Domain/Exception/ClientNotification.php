<?php

declare(strict_types=1);

namespace App\Shared\Domain\Exception;

/**
 * Interface for domains exceptions that handle all domain violations variants
 * that must be returned to the presentation layer. In a real application you should
 * return a translation key instead of a message.
 */
interface ClientNotification
{
    /**
     * Returns a user readable error messages
     */
    public function getUserNotification(): string;

    /**
     * Returns an identifier or class name of the exception origin
     */
    public function getOrigin(): string;
}
