<?php

declare(strict_types=1);

namespace App\Shared\Domain\Model;

/**
 * Abstract base class for entity identifiers
 */
abstract class Identifier
{
    private string $identifier;

    protected function __construct(string $identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Named constructor to create an instance based on the given string
     */
    abstract public static function fromString(string $identifier): self;

    /**
     * Returns the string representation
     */
    public function __toString(): string
    {
        return $this->identifier;
    }

    /**
     * Compares the given identifier object
     */
    public function equals(Identifier $otherIdentifier): bool
    {
        if (get_class($otherIdentifier) !== get_class($this)) {
            return false;
        }

        return  $this->__toString() === $otherIdentifier->__toString();
    }
}
