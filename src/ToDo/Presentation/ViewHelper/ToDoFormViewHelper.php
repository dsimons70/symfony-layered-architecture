<?php

declare(strict_types=1);

namespace App\ToDo\Presentation\ViewHelper;

use App\Shared\Presentation\ViewHelper\NotificationsViewHelper;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\User\Domain\Model\UserIdentifier;
use ArrayIterator;
use IteratorAggregate;
use Symfony\Component\HttpFoundation\Request;

/**
 * View helper implementation for "todo" forms.
 * This pattern helps you to keep your controller thin, your template clean and your front end guy's happy ;-)
 *
 * @see https://www.javaguides.net/2018/08/view-helper-design-pattern-in-java.html
 */
class ToDoFormViewHelper implements IteratorAggregate
{
    private string $scheduleDate = "";
    private string $toDoIdentifier = "";
    private array $tasks = [];
    private string $userIdentifier = "";
    private ?NotificationsViewHelper $notifications = null;

    /**
     * Named constructor to init the helper based on the given ToDo-object
     */
    public static function fromToDo(ToDo $toDo): ToDoFormViewHelper
    {
        $instance                 = new self();
        $instance->scheduleDate   = (string) $toDo->getScheduledDate();
        $instance->toDoIdentifier = (string) $toDo->getIdentifier();
        $instance->userIdentifier = (string) $toDo->getUserIdentifier();

        foreach ($toDo->getScheduledTasks() as $task) {
            $key = (string) $task->getTaskIdentifier();

            $instance->tasks[$key]['time']        = (string) $task->getScheduledTime();
            $instance->tasks[$key]['description'] = $task->getDescription();

            if ($task->isDone()) {
                $instance->tasks[$key]['is_done'] = $task->isDone();
            };
        }

        return $instance;
    }

    /**
     * First init based on the given identifiers
     */
    public static function fromIdentifiers(
        ToDoIdentifier $toDoIdentifier,
        TaskIdentifier $taskIdentifier,
        UserIdentifier $userIdentifier
    ): ToDoFormViewHelper {
        $instance                 = new self();
        $instance->toDoIdentifier = (string) $toDoIdentifier;
        $instance->userIdentifier = (string) $userIdentifier;

        $instance->addTask($taskIdentifier);

        return $instance;
    }

    /**
     * Restore based on the previous request
     */
    public static function fromRequest(
        Request $request,
        NotificationsViewHelper $notifications = null
    ): ToDoFormViewHelper {
        $instance                 = new self();
        $instance->scheduleDate   = $request->get('scheduleDate');
        $instance->toDoIdentifier = $request->get('toDoIdentifier');
        $instance->userIdentifier = $request->get('userIdentifier');
        $instance->tasks          = $request->get('tasks');
        $instance->notifications  = $notifications;

        return $instance;
    }

    /**
     * Register a new task for the form
     */
    public function addTask(TaskIdentifier $taskIdentifier): void
    {
        $identifier = (string) $taskIdentifier;

        $this->tasks[$identifier]['time']        = '';
        $this->tasks[$identifier]['description'] = '';
    }

    /**
     * Return the given css error class string if we find a violation for the
     * given field key or a empty string if we cant't find a violation
     */
    public function errorClass(string $key, string $errorClass): string
    {
        if (!$this->notifications instanceof NotificationsViewHelper) {
            return '';
        }

        if ($this->notifications->hasViolation($key)) {
            return $errorClass;
        }

        return '';
    }

    /**
     * @return string
     */
    public function getScheduleDate(): ?string
    {
        return $this->scheduleDate;
    }

    /**
     * @return string
     */
    public function getToDoIdentifier(): string
    {
        return $this->toDoIdentifier;
    }

    /**
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    /**
     * @return NotificationsViewHelper
     */
    public function getNotifications(): ?NotificationsViewHelper
    {
        return $this->notifications;
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new ArrayIterator($this->tasks);
    }
}
