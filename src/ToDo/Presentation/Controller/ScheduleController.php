<?php

declare(strict_types=1);

namespace App\ToDo\Presentation\Controller;

use App\Shared\Domain\Exception\ClientNotification;
use App\Shared\Presentation\ViewHelper\NotificationsViewHelper;
use App\ToDo\Application\Schedule\ScheduleDto;
use App\ToDo\Application\Schedule\ToDoScheduleService;
use App\ToDo\Presentation\ViewHelper\ToDoFormViewHelper;
use App\User\Infrastructure\Session\SessionRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ScheduleController extends AbstractController
{
    private SessionRegistry $sessionRegistry;
    private ToDoScheduleService $toDoScheduleService;
    private ValidatorInterface $validator;

    public function __construct(
        SessionRegistry $session,
        ToDoScheduleService $toDoSchedulerService,
        ValidatorInterface $validator
    ) {
        $this->sessionRegistry     = $session;
        $this->toDoScheduleService = $toDoSchedulerService;
        $this->validator           = $validator;
    }

    /**
     * @Route("/schedule", name="schedule_todo")
     * @throws Exception
     */
    public function schedule(Request $request, ScheduleDto $scheduleDto): Response
    {
        if ($request->get('formAction') == 'scheduleToDo') {
            return $this->handleSchedule($request, $scheduleDto);
        }

        if ($request->get('formAction') == 'addTask') {
            return $this->addTaskToForm($request);
        }

        if ($request->get('formAction') == 'cancel') {
            return $this->redirectToRoute('list_todos');
        }

        return $this->initForm();
    }

    /**
     * Handle the form processing
     * @throws Exception
     */
    private function handleSchedule(Request $request, ScheduleDto $scheduleDto)
    {
        $violationList = $this->validator->validate($scheduleDto);

        if (count($violationList) > 0) {
            $notifications = NotificationsViewHelper::fromConstraintViolationList($violationList);
            $formHelper    = ToDoFormViewHelper::fromRequest($request, $notifications);

            return $this->render('@todo/schedule.html.twig', ['formHelper' => $formHelper]);
        }

        try {
            $this->toDoScheduleService->schedule($scheduleDto);
        } catch (ClientNotification $notificationException) {
            $notifications = NotificationsViewHelper::fromNotificationException($notificationException);
            $formHelper    = ToDoFormViewHelper::fromRequest($request, $notifications);

            return $this->render('@todo/schedule.html.twig', ['formHelper' => $formHelper]);
        }

        return $this->redirectToRoute('list_todos');
    }

    /**
     * Return response for a initialized form template
     * @throws Exception
     */
    private function initForm(): Response
    {
        $formHelper = ToDoFormViewHelper::fromIdentifiers(
            $this->toDoScheduleService->nextToDoIdentifier(),
            $this->toDoScheduleService->nextTaskIdentifier(),
            $this->sessionRegistry->getUser()->getIdentifier()
        );

        return $this->render('@todo/schedule.html.twig', ['formHelper' => $formHelper]);
    }

    /**
     * Return response to update the form with a new task
     * @throws Exception
     */
    private function addTaskToForm(Request $request): Response
    {
        $formHelper = ToDoFormViewHelper::fromRequest($request);
        $formHelper->addTask($this->toDoScheduleService->nextTaskIdentifier());

        return $this->render('@todo/schedule.html.twig', ['formHelper' => $formHelper]);
    }
}
