<?php

declare(strict_types=1);

namespace App\ToDo\Presentation\Controller;

use App\ToDo\Application\Remove\RemoveDto;
use App\ToDo\Application\Remove\ToDoRemoveService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RemoveController extends AbstractController
{
    private ToDoRemoveService $toDoRemoveService;
    private ValidatorInterface $validator;

    public function __construct(ToDoRemoveService $toDoRemoveService, ValidatorInterface $validator)
    {
        $this->toDoRemoveService = $toDoRemoveService;
        $this->validator         = $validator;
    }

    /**
     * @Route("/remove", name="remove_todo")
     * @throws Exception
     */
    public function remove(RemoveDto $removeToDo): Response
    {
        $errors = $this->validator->validate($removeToDo);

        if (count($errors) > 0) {
            throw new Exception('Identifier is invalid');
        }

        $message = $this->toDoRemoveService->remove($removeToDo);
        $this->addFlash('success', $message);

        return $this->redirectToRoute('list_todos');
    }
}
