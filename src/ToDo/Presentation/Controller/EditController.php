<?php

declare(strict_types=1);

namespace App\ToDo\Presentation\Controller;

use App\Shared\Domain\Exception\ClientNotification;
use App\Shared\Presentation\ViewHelper\NotificationsViewHelper;
use App\ToDo\Application\Edit\EditDto;
use App\ToDo\Application\Edit\ToDoEditService;
use App\ToDo\Presentation\ViewHelper\ToDoFormViewHelper;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{
    private ToDoEditService $toDoEditService;

    public function __construct(ToDoEditService $toDoEditService)
    {
        $this->toDoEditService = $toDoEditService;
    }

    /**
     * @Route("/edit", name="edit_todo")
     * @throws Exception
     */
    public function edit(Request $request, EditDto $editToDo): Response
    {
        if ($request->get('formAction') === 'editToDo') {
            return $this->handleEdit($request, $editToDo);
        }

        if ($request->get('formAction') === 'addTask') {
            return $this->addTaskToForm($request);
        }

        if ($request->get('formAction') === 'cancel') {
            return $this->redirectToRoute('list_todos');
        }

        return $this->initForm($request);
    }

    /**
     * Handles the edit form
     * @throws Exception
     */
    private function handleEdit(Request $request, EditDto $editDto): Response
    {
        try {
            $this->toDoEditService->edit($editDto);
        } catch (ClientNotification $notificationException) {
            $notifications = NotificationsViewHelper::fromNotificationException($notificationException);
            $formHelper = ToDoFormViewHelper::fromRequest($request, $notifications);

            return $this->render('@todo/edit.html.twig', ['formHelper' => $formHelper]);
        }

        return $this->redirectToRoute('list_todos');
    }

    /**
     * Return response for a initialized and filled form template
     * @throws Exception
     */
    private function initForm(Request $request): Response
    {
        $identifier = $request->get('toDoIdentifier');
        $toDo = $this->toDoEditService->getToDo($identifier);
        $viewHelper = ToDoFormViewHelper::fromToDo($toDo);

        return $this->render('@todo/edit.html.twig', ['formHelper' => $viewHelper]);
    }

    /**
     * Return response to update the form with a new task
     * @throws Exception
     */
    private function addTaskToForm(Request $request): Response
    {
        $viewHelper = ToDoFormViewHelper::fromRequest($request);
        $viewHelper->addTask($this->toDoEditService->nextTaskIdentifier());

        return $this->render('@todo/edit.html.twig', ['formHelper' => $viewHelper]);
    }
}
