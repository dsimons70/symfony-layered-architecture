<?php

declare(strict_types=1);

namespace App\ToDo\Presentation\Controller;

use App\ToDo\Application\Overview\OverviewDto;
use App\ToDo\Application\Overview\ToDoOverviewService;
use App\User\Infrastructure\Session\SessionRegistry;
use Exception;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OverviewController extends AbstractController
{
    private ToDoOverviewService $toDoOverviewService;
    private SessionRegistry $session;
    private ValidatorInterface $validator;

    public function __construct(
        ToDoOverviewService $doOverviewService,
        SessionRegistry $session,
        ValidatorInterface $validator
    ) {
        $this->toDoOverviewService = $doOverviewService;
        $this->session             = $session;
        $this->validator           = $validator;
    }

    /**
     * @Route("/list", name="list_todos")
     * @throws Exception
     */
    public function list(): Response
    {
        $user         = $this->session->getUser();
        $toDoOverview = new OverviewDto((string) $user->getIdentifier());
        $errors       = $this->validator->validate($toDoOverview);

        if (count($errors) > 0) {
            $message = $errors->get(0)->getMessage();
            throw new RuntimeException($message);
        }

        $todoList = $this->toDoOverviewService->list($toDoOverview);

        $params = [
            "toDoList" => $todoList,
            "username" => $this->session->getUser()->getCredentials()->getUsername(),
            'errors'   => $errors,
        ];

        return $this->render('@todo/overview.html.twig', $params);
    }
}
