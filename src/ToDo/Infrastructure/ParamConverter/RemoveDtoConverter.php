<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\ParamConverter;

use App\ToDo\Application\Remove\RemoveDto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class RemoveDtoConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param      = $configuration->getName();
        $removeToDo = new RemoveDto($request->get('toDoIdentifier'));

        $request->attributes->set($param, $removeToDo);
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === RemoveDto::class;
    }
}
