<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\ParamConverter;

use App\ToDo\Application\Edit\EditDto;
use App\ToDo\Application\Edit\EditTaskDto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class EditDtoConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param          = $configuration->getName();
        $requestTasks   = $request->get('tasks', []);
        $scheduledTasks = [];

        foreach ($requestTasks as $taskIdentifier => $requestTask) {
            $scheduledTasks[] = new EditTaskDto(
                $taskIdentifier,
                $requestTask['time'],
                $requestTask['description'],
                isset($requestTask['is_done']) ? true : false
            );
        }

        $scheduleToDo = new EditDto(
            $request->get('toDoIdentifier', ''),
            $request->get('userIdentifier', ''),
            $request->get('scheduleDate', ''),
            $scheduledTasks
        );

        $request->attributes->set($param, $scheduleToDo);
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === EditDto::class;
    }
}
