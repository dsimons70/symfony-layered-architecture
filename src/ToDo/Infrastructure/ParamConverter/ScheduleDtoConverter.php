<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\ParamConverter;

use App\ToDo\Application\Schedule\ScheduleDto;
use App\ToDo\Application\Schedule\ScheduleTaskDto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ScheduleDtoConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $param          = $configuration->getName();
        $scheduledTasks = [];
        $requestTasks   = $request->get('tasks', []);

        foreach ($requestTasks as $taskIdentifier => $requestTask) {
            $scheduledTasks[] = new ScheduleTaskDto(
                $taskIdentifier,
                $requestTask['time'],
                $requestTask['description'],
            );
        }

        $scheduleToDo = new ScheduleDto(
            $request->get('toDoIdentifier', ''),
            $request->get('userIdentifier', ''),
            $request->get('scheduleDate', ''),
            $scheduledTasks
        );

        $request->attributes->set($param, $scheduleToDo);
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === ScheduleDto::class;
    }
}
