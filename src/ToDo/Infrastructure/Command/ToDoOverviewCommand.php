<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\Command;

use App\ToDo\Application\Overview\OverviewDto;
use App\ToDo\Application\Overview\ToDoOverviewService;
use App\ToDo\Domain\Model\ScheduledTask;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToDoOverviewCommand extends Command
{
    private ToDoOverviewService $toDoOverviewService;

    /** @inheritDoc */
    protected static $defaultName = 'app:todo:overview';

    public function __construct(ToDoOverviewService $toDoOverviewService)
    {
        $this->toDoOverviewService = $toDoOverviewService;
        parent::__construct();
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this->setDescription("Console command to list all todo's for the given username");
        $this->addArgument('userIdentifier', InputArgument::REQUIRED, 'Please provide a valid userIdentifier');
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $showToDoOverview = new OverviewDto($input->getArgument('userIdentifier'));
        $toDoOverview     = $this->toDoOverviewService->list($showToDoOverview);

        $output->writeln("We found (" . count($toDoOverview) . ") to-dos for user:" . $input->getArgument('userIdentifier'));

        foreach ($toDoOverview as $toDo) {
            $output->writeln("Date:" . $toDo->getScheduledDate() . " Identifier:" . $toDo->getIdentifier());

            /** @var ScheduledTask $task */
            foreach ($toDo as $task) {
                $status = $task->isDone() ? 'Done' : 'Pending';

                $output->writeln(
                    " - Time:" . $task->getScheduledTime() . ' Description:' . $task->getDescription() . ' Status:' . $status
                );
            }
        }
    }
}
