<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\MySql\MySqlConnection;
use App\Shared\Infrastructure\Persistence\MySql\MySqlPersistenceType;
use App\Shared\Infrastructure\Persistence\PersistenceIdentification;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoList;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserIdentifier;
use Exception;
use PDO;
use PDOException;
use Ramsey\Uuid\Uuid;
use RuntimeException;

/**
 * ToDoRepository implementation for MySql databases
 *
 * We following the 'Interface segregation principle' from SOLID to keep our
 * domain layer repositories clean from infrastructure dependencies
 */
class MySqlToDoRepository implements ToDoRepository, PersistenceIdentification
{
    private PDO $pdo;

    /**
     * @throws RuntimeException
     */
    public function __construct(MySqlConnection $pdoConnectionFactory)
    {
        $this->pdo = $pdoConnectionFactory->connect();
    }

    /**
     * @inheritDoc
     */
    public function add(ToDo $toDo): void
    {
        $this->pdo->beginTransaction();

        try {
            $stmd = $this->pdo->prepare(
                <<<SQL
                INSERT INTO todo.ToDos (identifier, user_identifier, schedule_date) 
                VALUES (:identifier, :user_identifier, :schedule_date)
                SQL
            );

            $stmd->bindValue('identifier', $toDo->getIdentifier(), PDO::PARAM_STR);
            $stmd->bindValue('user_identifier', $toDo->getUserIdentifier(), PDO::PARAM_STR);
            $stmd->bindValue('schedule_date', $toDo->getScheduledDate(), PDO::PARAM_STR);
            $stmd->execute();

            $toDoId = $this->pdo->lastInsertId();

            foreach ($toDo->getScheduledTasks() as $task) {
                $this->insertTask($task, $toDoId);
            }
        } catch (PDOException $exception) {
            $this->pdo->rollBack();
            throw $exception;
        }

        $this->pdo->commit();
    }

    /**
     * @inheritDoc
     */
    public function replace(ToDo $toDo): void
    {
        $this->pdo->beginTransaction();

        try {
            // Update ToDos
            $stmd = $this->pdo->prepare("UPDATE todo.ToDos set schedule_date = :schedule_date WHERE identifier = :identifier");
            $stmd->bindValue('schedule_date', $toDo->getScheduledDate(), PDO::PARAM_STR);
            $stmd->bindValue('identifier', $toDo->getIdentifier(), PDO::PARAM_STR);
            $stmd->execute();

            // Retrieve todo_id
            $stmd = $this->pdo->prepare("SELECT todo_id FROM todo.ToDos WHERE identifier = :identifier");
            $stmd->bindValue('identifier', $toDo->getIdentifier(), PDO::PARAM_STR);
            $stmd->execute();

            $toDoId = $stmd->fetchColumn(0);

            // Delete all old Tasks
            $stmd = $this->pdo->prepare("DELETE FROM todo.Tasks WHERE todo_id = :todo_id");
            $stmd->bindValue('todo_id', $toDoId, PDO::PARAM_INT);
            $stmd->execute();

            // Insert updated or new Tasks
            foreach ($toDo->getScheduledTasks() as $task) {
                $this->insertTask($task, $toDoId);
            }
        } catch (PDOException $exception) {
            $this->pdo->rollBack();
            throw $exception;
        }

        $this->pdo->commit();
    }

    /**
     * @inheritDoc
     */
    public function remove(ToDo $toDo): void
    {
        $this->pdo->beginTransaction();

        try {
            $stmd = $this->pdo->prepare("DELETE FROM  todo.ToDos WHERE identifier = :identifier");
            $stmd->bindValue('identifier', $toDo->getIdentifier(), PDO::PARAM_STR);
            $stmd->execute();
        } catch (PDOException $exception) {
            $this->pdo->rollBack();
            throw $exception;
        }

        $this->pdo->commit();
    }

    /**
     * @inheritDoc
     */
    public function findByUserIdentifier(UserIdentifier $userIdentifier): ToDoList
    {
        $stmd = $this->pdo->prepare(
            <<<SQL
            SELECT td.identifier AS todo_identifier, td.user_identifier, td.schedule_date, t.identifier AS task_identifier, 
                   time_format(t.schedule_time, '%H:%i') AS schedule_time, t.description, t.is_done
            FROM  todo.ToDos AS td
            INNER JOIN  todo.Tasks AS t ON td.todo_id = t.todo_id
            WHERE td.user_identifier = :user_identifier
            ORDER BY td.schedule_date, t.schedule_time
            SQL
        );

        $stmd->bindValue('user_identifier', strval($userIdentifier), PDO::PARAM_STR);
        $stmd->execute();

        $result = $stmd->fetchAll(PDO::FETCH_ASSOC);
        $toDos  = [];

        foreach ($result as $row) {
            if (!isset($toDos[$row['todo_identifier']])) {
                $toDos[$row['todo_identifier']] = $this->createToDoInstance($row);
            }

            $toDos[$row['todo_identifier']]->scheduleTask($this->createTaskInstance($row));
        }

        return ToDoList::fromArray($toDos);
    }

    /**
     * @inheritDoc
     */
    public function findByIdentifier(ToDoIdentifier $toDoIdentifier): ?ToDo
    {
        $stmd = $this->pdo->prepare(
            <<<SQL
            SELECT td.identifier AS todo_identifier, td.user_identifier, td.schedule_date, t.identifier AS task_identifier, 
                   time_format(t.schedule_time, '%H:%i') AS schedule_time, t.description, t.is_done
            FROM  todo.ToDos AS td
            INNER JOIN  todo.Tasks AS t ON td.todo_id = t.todo_id
            WHERE td.identifier = :identifier
            ORDER BY td.schedule_date, t.schedule_time
            SQL
        );

        $stmd->bindValue('identifier', $toDoIdentifier, PDO::PARAM_STR);
        $stmd->execute();

        $toDo = null;

        foreach ($stmd->fetchAll(PDO::FETCH_ASSOC) as $row) {
            if (!($toDo instanceof ToDo)) {
                $toDo = $this->createToDoInstance($row);
            }

            $toDo->scheduleTask($this->createTaskInstance($row));
        }

        return $toDo;
    }

    /**
     * @inheritDoc
     */
    public function nextToDoIdentifier(): ToDoIdentifier
    {
        try {
            $uuid4 = Uuid::uuid4();
        } catch (Exception $exception) {
            throw new RuntimeException("Can't generate identifier", 0, $exception);
        }

        return ToDoIdentifier::fromString($uuid4->toString());
    }

    /**
     * @inheritDoc
     */
    public function nextTaskIdentifier(): TaskIdentifier
    {
        try {
            $uuid4 = Uuid::uuid4();
        } catch (Exception $exception) {
            throw new RuntimeException("Can't generate identifier", 0, $exception);
        }

        return TaskIdentifier::fromString($uuid4->toString());
    }

    /**
     * @inheritDoc
     */
    public function getPersistenceType(): PersistenceType
    {
        return new MySqlPersistenceType();
    }

    /**
     * @inheritDoc
     */
    public function getImplementedRepositoryInterface(): string
    {
        return ToDoRepository::class;
    }

    /**
     * Factory for ToDoEntry
     * @throws ToDoRuntimeException
     */
    private function createToDoInstance(array $row): ToDo
    {
        $toDo = ToDo::scheduleFor(
            UserIdentifier::fromString($row['user_identifier']),
            ScheduledDate::fromString($row['schedule_date']),
            ToDoIdentifier::fromString($row['todo_identifier'])
        );

        return $toDo;
    }

    /**
     * Factory for ScheduledTask
     * @throws ToDoRuntimeException
     */
    private function createTaskInstance(array $row): ScheduledTask
    {
        $scheduledTask = ScheduledTask::schedule(
            TaskIdentifier::fromString($row['task_identifier']),
            ScheduledTime::fromString($row['schedule_time']),
            $row['description']
        );

        if (intval($row['is_done']) === 1) {
            $scheduledTask->markAsDone();
        }

        return $scheduledTask;
    }

    /**
     * Insert the the Task for the given todo_id
     * @throws PDOException
     */
    private function insertTask(ScheduledTask $task, string $toDoId): void
    {
        $stmd = $this->pdo->prepare(
            <<<SQL
            INSERT INTO  todo.Tasks (identifier, todo_id, schedule_time, description, is_done) 
            VALUES (:identifier, :todo_id,:schedule_time, :description, :is_done)
            SQL
        );

        $stmd->bindValue('identifier', $task->getTaskIdentifier(), PDO::PARAM_STR);
        $stmd->bindValue('todo_id', $toDoId, PDO::PARAM_INT);
        $stmd->bindValue('schedule_time', $task->getScheduledTime(), PDO::PARAM_STR);
        $stmd->bindValue('description', $task->getDescription());
        $stmd->bindValue('is_done', $task->isDone(), PDO::PARAM_BOOL);
        $stmd->execute();
    }
}
