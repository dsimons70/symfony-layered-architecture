<?php

declare(strict_types=1);

namespace App\ToDo\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\PersistenceIdentification;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\Shared\Infrastructure\Persistence\Redis\RedisConnection;
use App\Shared\Infrastructure\Persistence\Redis\RedisPersistenceType;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoList;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserIdentifier;
use Redis;
use RuntimeException;

/**
 * ToDoRepository implementation for redis databases
 *
 * We following the 'Interface segregation principle' from SOLID to keep our
 * domain layer repositories clean from infrastructure dependencies
 *
 * Redis layout:
 *
 * todos:1                => json data                    (key => value)
 * todos:2                => json data                    (key => value)
 * todos:user:1           => [date] => [todos:1, todos:2] (key => sorted set)
 * todos:identifier       => last todo-id                 (INCR)
 * todos:identifier:tasks => last task-id                 (INCR)
 *
 */
class RedisToDoRepository implements ToDoRepository, PersistenceIdentification
{
    private Redis $redis;

    /**
     * @throws RuntimeException
     */
    public function __construct(RedisConnection $redisConnection)
    {
        $this->redis = $redisConnection->connect();
    }

    /**
     * @inheritDoc
     */
    public function findByUserIdentifier(UserIdentifier $userIdentifier): ToDoList
    {
        $toDoIdentifiers = $this->redis->zRange($this->generateUserToDoListKey($userIdentifier), 0, -1);
        $toDos           = [];

        foreach ($toDoIdentifiers as $toDoIdentifier) {
            $json = $this->redis->get($toDoIdentifier);

            if ($json === false) {
                throw new RuntimeException("Can'f find data for identifier: $toDoIdentifier");
            }

            $toDos[] = $this->decodeFromJson($json);
        }

        return ToDoList::fromArray($toDos);
    }

    /**
     * @inheritDoc
     */
    public function findByIdentifier(ToDoIdentifier $toDoIdentifier): ?ToDo
    {
        $json = $this->redis->get(strval($toDoIdentifier));

        if ($json === false) {
            return null;
        }

        return $this->decodeFromJson($json);
    }

    /**
     * @inheritDoc
     */
    public function add(ToDo $toDo): void
    {
        $userToDoListIdentifier = $this->generateUserToDoListKey($toDo->getUserIdentifier());
        $sortKey                = $toDo->getScheduledDate()->toTimestamp();
        $nextDoIdentifier       = (string) $toDo->getIdentifier();

        $addResult = $this->redis->zAdd($userToDoListIdentifier, [], $sortKey, $nextDoIdentifier);
        $setResult = $this->redis->set($nextDoIdentifier, $this->encodeToJson($toDo));

        if ($addResult === 0 || $setResult === false) {
            throw new RuntimeException('Unable to add to to-to');
        }
    }

    /**
     * @inheritDoc
     */
    public function replace(ToDo $toDo): void
    {
        $identifier = (string) $toDo->getIdentifier();

        $delResult = $this->redis->del($identifier);
        $setResult = $this->redis->set($identifier, $this->encodeToJson($toDo));

        if ($delResult === 0 || $setResult === false) {
            throw new RuntimeException("Can't replace todo with identifier $identifier");
        }
    }

    /**
     * @inheritDoc
     */
    public function remove(ToDo $toDo): void
    {
        $toDoListIdentifier = $this->generateUserToDoListKey($toDo->getUserIdentifier());
        $toDoIdentifier     = (string) $toDo->getIdentifier();
        $delResult          = $this->redis->del($toDoIdentifier);
        $remResult          = $this->redis->zRem($toDoListIdentifier, $toDoIdentifier);

        if ($delResult === 0 || $remResult === 0) {
            throw new RuntimeException("Can't remove todo");
        }
    }

    /**
     * @inheritDoc
     */
    public function nextToDoIdentifier(): ToDoIdentifier
    {
        $incrId     = $this->redis->incr('todos:identifier');
        $identifier = 'todos:' . $incrId;

        return ToDoIdentifier::fromString($identifier);
    }

    /**
     * @inheritDoc
     */
    public function nextTaskIdentifier(): TaskIdentifier
    {
        $incrId     = $this->redis->incr('todos:tasks:identifier');
        $identifier = 'tasks:' . $incrId;

        return TaskIdentifier::fromString($identifier);
    }

    /**
     * @inheritDoc
     */
    public function getPersistenceType(): PersistenceType
    {
        return new RedisPersistenceType();
    }

    /**
     * @inheritDoc
     */
    public function getImplementedRepositoryInterface(): string
    {
        return ToDoRepository::class;
    }

    /**
     * Generates the key for user specific todo-lists
     */
    private function generateUserToDoListKey(UserIdentifier $userIdentifier): string
    {
        return 'todos:' . strval($userIdentifier);
    }

    /**
     * Encode the given item to a json string
     * @throws RuntimeException
     */
    private function encodeToJson(ToDo $toDo): string
    {
        $tasks = [];

        /** @var ScheduledTask $task */
        foreach ($toDo as $task) {
            $tasks[] = [
                'identifier'     => (string) $task->getTaskIdentifier(),
                'scheduled_time' => (string) $task->getScheduledTime(),
                'description'    => $task->getDescription(),
                'is_done'        => $task->isDone(),
            ];
        }

        $data = [
            'identifier'      => (string) $toDo->getIdentifier(),
            'scheduled_date'  => (string) $toDo->getScheduledDate(),
            'user_identifier' => (string) $toDo->getUserIdentifier(),
            'tasks'           => $tasks,
        ];

        $encoded = json_encode($data);

        if ($encoded === false) {
            throw new RuntimeException("Can't encode todo to json");
        }

        return $encoded;
    }

    /**
     * Decodes the json to ToDo-Entity
     * @throws RuntimeException
     */
    private function decodeFromJson(string $json): ToDo
    {
        $row = json_decode($json, true);

        if ($row === null) {
            throw new RuntimeException("Can't decode todo from jason");
        }

        $toDo = ToDo::scheduleFor(
            UserIdentifier::fromString($row['user_identifier']),
            ScheduledDate::fromString($row['scheduled_date']),
            ToDoIdentifier::fromString($row['identifier'])
        );

        foreach ($row['tasks'] as $task) {
            $scheduledTask = ScheduledTask::schedule(
                TaskIdentifier::fromString($task['identifier']),
                ScheduledTime::fromString($task['scheduled_time']),
                $task['description']
            );

            if (intval($task['is_done']) === 1) {
                $scheduledTask->markAsDone();
            }

            $toDo->scheduleTask($scheduledTask);
        }

        return $toDo;
    }
}
