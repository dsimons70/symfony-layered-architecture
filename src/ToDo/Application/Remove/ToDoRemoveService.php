<?php

declare(strict_types=1);

namespace App\ToDo\Application\Remove;

use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use RuntimeException;

class ToDoRemoveService
{
    private ToDoRepository $toDoRepository;

    public function __construct(ToDoRepository $toDoRepository)
    {
        $this->toDoRepository = $toDoRepository;
    }

    /**
     * Remove item with the given identifier from the collection
     * @throws RuntimeException
     */
    public function remove(RemoveDto $removeDto): string
    {
        $toDoIdentifier = ToDoIdentifier::fromString($removeDto->getToDoIdentifier());
        $toDo           = $this->toDoRepository->findByIdentifier($toDoIdentifier);

        if ($toDo === null) {
            return "ToDo does not exists in your list";
        }

        try {
            $this->toDoRepository->remove($toDo);
        } catch (RuntimeException $exception) {
            return "Error during removal";
        }

        return $this->composeSuccessMessage($toDo);
    }

    /**
     * Returns the composed success message
     */
    private function composeSuccessMessage(ToDo $toDo): string
    {
        $cntTasks     = count($toDo);
        $scheduleDate = (string) $toDo->getScheduledDate();
        $message      = "Item for the $scheduleDate and its $cntTasks assigned tasks has been removed from your list";

        return $message;
    }
}
