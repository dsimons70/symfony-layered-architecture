<?php

declare(strict_types=1);

namespace App\ToDo\Application\Remove;

class RemoveDto
{
    private string $toDoIdentifier;

    public function __construct(string $toDoIdentifier)
    {
        $this->toDoIdentifier = trim($toDoIdentifier);
    }

    /**
     * Return the ToDo identifier
     */
    public function getToDoIdentifier(): string
    {
        return $this->toDoIdentifier;
    }
}
