<?php

declare(strict_types=1);

namespace App\ToDo\Application\Schedule;

class ScheduleDto
{
    private string $toDoIdentifier;
    private string $userIdentifier;
    private string $scheduledDate;

    /** @var ScheduleTaskDto[] */
    private array $tasks = [];

    /**
     * @param ScheduleTaskDto[] $scheduledTasks
     */
    public function __construct(
        string $toDoIdentifier,
        string $userIdentifier,
        string $scheduledDate,
        array $scheduledTasks
    ) {
        $this->toDoIdentifier = trim($toDoIdentifier);
        $this->userIdentifier = trim($userIdentifier);
        $this->scheduledDate  = trim($scheduledDate);

        foreach ($scheduledTasks as $scheduleTask) {
            $this->addTask($scheduleTask);
        }
    }

    /**
     * Returns the userIdentifier
     */
    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    /**
     * Returns the schedule date
     */
    public function getScheduledDate(): string
    {
        return $this->scheduledDate;
    }

    /**
     * @return string
     */
    public function getToDoIdentifier(): string
    {
        return $this->toDoIdentifier;
    }

    /**
     * @return ScheduleTaskDto[]
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * Function to ensure type safety
     */
    private function addTask(ScheduleTaskDto $scheduledTask):void
    {
        $this->tasks[$scheduledTask->getTaskIdentifier()] = $scheduledTask;
    }
}
