<?php

declare(strict_types=1);

namespace App\ToDo\Application\Schedule;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserIdentifier;
use RuntimeException;

class ToDoScheduleService
{
    private ToDoRepository $toDoRepository;

    public function __construct(ToDoRepository $toDoRepository)
    {
        $this->toDoRepository = $toDoRepository;
    }

    /**
     * Schedule a new "todo" with tasks
     * @throws RuntimeException
     */
    public function schedule(ScheduleDto $scheduleDto): ToDo
    {
        $scheduledDate = ScheduledDate::fromString($scheduleDto->getScheduledDate());

        if ($scheduledDate->isInThePast()) {
            throw ToDoRuntimeException::forDatesInPast($scheduledDate);
        }

        $toDo = ToDo::scheduleFor(
            UserIdentifier::fromString($scheduleDto->getUserIdentifier()),
            $scheduledDate,
            ToDoIdentifier::fromString($scheduleDto->getToDoIdentifier())
        );

        foreach ($scheduleDto->getTasks() as $scheduleTask) {
            $toDo->scheduleTask($this->createTask($scheduleTask));
        }

        $this->toDoRepository->add($toDo);
        return $toDo;
    }

    /**
     * Return the next generated identifier
     * @throws RuntimeException
     */
    public function nextToDoIdentifier(): ToDoIdentifier
    {
        return $this->toDoRepository->nextToDoIdentifier();
    }

    /**
     * Return the next generated identifier
     * @throws RuntimeException
     */
    public function nextTaskIdentifier(): TaskIdentifier
    {
        return $this->toDoRepository->nextTaskIdentifier();
    }

    /**
     * Create a task model from the given dto
     * @throws ToDoRuntimeException
     */
    private function createTask(ScheduleTaskDto $taskDto): ScheduledTask
    {
        $scheduleTime   = ScheduledTime::fromString($taskDto->getScheduledTime());
        $taskIdentifier = TaskIdentifier::fromString($taskDto->getTaskIdentifier());

        return ScheduledTask::schedule(
            $taskIdentifier,
            $scheduleTime,
            $taskDto->getDescription()
        );
    }
}
