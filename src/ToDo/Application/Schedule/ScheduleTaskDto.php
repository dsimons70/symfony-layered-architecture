<?php

declare(strict_types=1);

namespace App\ToDo\Application\Schedule;

class ScheduleTaskDto
{
    private string $scheduledTime;
    private string $description;
    private string $taskIdentifier;

    public function __construct(string $taskIdentifier, string $scheduledTime, string $description)
    {
        $this->taskIdentifier = trim($taskIdentifier);
        $this->scheduledTime  = trim($scheduledTime);
        $this->description    = trim($description);
    }

    /**
     * Returns the time string
     */
    public function getScheduledTime(): string
    {
        return $this->scheduledTime;
    }

    /**
     * Returns the description
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Return the identifier
     */
    public function getTaskIdentifier(): string
    {
        return $this->taskIdentifier;
    }
}
