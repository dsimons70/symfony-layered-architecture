<?php

declare(strict_types=1);

namespace App\ToDo\Application\Overview;

use App\ToDo\Domain\Model\ToDoList;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserIdentifier;
use RuntimeException;

class ToDoOverviewService
{
    private ToDoRepository $toDoRepository;

    public function __construct(ToDoRepository $toDoRepository)
    {
        $this->toDoRepository = $toDoRepository;
    }

    /**
     * List all toDo's for the given UserIdentifier
     * @throws RuntimeException
     */
    public function list(OverviewDto $overviewDto): ToDoList
    {
        $userIdentifier = UserIdentifier::fromString($overviewDto->getUserIdentifier());
        $toDoList = $this->toDoRepository->findByUserIdentifier($userIdentifier);

        return $toDoList;
    }
}
