<?php

declare(strict_types=1);

namespace App\ToDo\Application\Overview;

class OverviewDto
{
    private string $userIdentifier = '';

    public function __construct(?string $userIdentifier)
    {
        $this->userIdentifier = trim($userIdentifier);
    }

    /**
     * Returns the userIdentifier
     */
    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }
}
