<?php

declare(strict_types=1);

namespace App\ToDo\Application\Edit;

class EditTaskDto
{
    private string $taskIdentifier;
    private string $scheduledTime;
    private string $description;
    private bool $isDone;

    public function __construct(string $taskIdentifier, string $scheduledTime, string $description, bool $isDone)
    {
        $this->taskIdentifier = trim($taskIdentifier);
        $this->scheduledTime  = trim($scheduledTime);
        $this->description    = trim($description);
        $this->isDone         = $isDone;
    }

    /**
     * Return the identifier
     */
    public function getTaskIdentifier(): string
    {
        return $this->taskIdentifier;
    }

    /**
     * Returns the time string
     */
    public function getScheduledTime(): string
    {
        return $this->scheduledTime;
    }

    /**
     * Returns the description
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Determine if the task is done
     */
    public function isDone(): bool
    {
        return $this->isDone;
    }
}
