<?php

declare(strict_types=1);

namespace App\ToDo\Application\Edit;

class EditDto
{
    private string $toDoIdentifier;
    private string $userIdentifier;
    private string $scheduledDate;

    /** @var EditTaskDto[] */
    private array $tasks = [];

    public function __construct(
        string $toDoIdentifier,
        string $userIdentifier,
        string $scheduledDate,
        array $assignedTasks
    ) {
        $this->userIdentifier = trim($userIdentifier);
        $this->scheduledDate  = trim($scheduledDate);
        $this->toDoIdentifier = trim($toDoIdentifier);

        foreach ($assignedTasks as $scheduleTask) {
            $this->addTask($scheduleTask);
        }
    }

    /**
     * Returns the userIdentifier
     */
    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    /**
     * Returns the schedule date
     */
    public function getScheduledDate(): string
    {
        return $this->scheduledDate;
    }

    /**
     * Returns the unique identifier
     */
    public function getToDoIdentifier(): string
    {
        return $this->toDoIdentifier;
    }

    /**
     * @return EditTaskDto[]
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * Function to ensure type safety
     */
    private function addTask(EditTaskDto $scheduledTask)
    {
        $this->tasks[$scheduledTask->getTaskIdentifier()] = $scheduledTask;
    }
}
