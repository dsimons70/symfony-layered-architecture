<?php

declare(strict_types=1);

namespace App\ToDo\Application\Edit;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserIdentifier;
use Exception;
use RuntimeException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ToDoEditService
{
    private ToDoRepository $toDoRepository;
    private ValidatorInterface $validator;

    public function __construct(ToDoRepository $toDoRepository, ValidatorInterface $validator)
    {
        $this->toDoRepository = $toDoRepository;
        $this->validator = $validator;
    }

    /**
     * Update the edited item
     * @throws Exception
     */
    public function edit(EditDto $editDto): void
    {
        $violationList = $this->validator->validate($editDto);

        if (count($violationList) > 0) {
           throw ToDoRuntimeException::fromConstraintViolationList($violationList);
        }

        $toDo = ToDo::scheduleFor(
            UserIdentifier::fromString($editDto->getUserIdentifier()),
            ScheduledDate::fromString($editDto->getScheduledDate()),
            ToDoIdentifier::fromString($editDto->getToDoIdentifier())
        );

        foreach ($editDto->getTasks() as $assignedTask) {
            $toDo->scheduleTask($this->createTask($assignedTask));
        }

        $this->toDoRepository->replace($toDo);
    }

    /**
     * Retrieve the item for the given identifier
     * @throws RuntimeException
     */
    public function getToDo(string $identifier): ToDo
    {
        $toDoIdentifier = ToDoIdentifier::fromString($identifier);
        return $this->toDoRepository->findByIdentifier($toDoIdentifier);
    }

    /**
     * Generate unique identifier for task
     * @throws Exception
     */
    public function nextTaskIdentifier(): TaskIdentifier
    {
        return $this->toDoRepository->nextTaskIdentifier();
    }

    /**
     * Creates the task model from the given dto
     * @throws ToDoRuntimeException
     */
    private function createTask(EditTaskDto $taskDto): ScheduledTask
    {
        $scheduledTask = ScheduledTask::schedule(
            TaskIdentifier::fromString($taskDto->getTaskIdentifier()),
            ScheduledTime::fromString($taskDto->getScheduledTime()),
            $taskDto->getDescription()
        );

        if ($taskDto->isDone()) {
            $scheduledTask->markAsDone();
        }

        return $scheduledTask;
    }
}
