<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Exception;

use App\Shared\Domain\Exception\ClientNotification;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use Exception;
use RuntimeException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ToDoRuntimeException extends RuntimeException implements ClientNotification
{
    private string $userNotification = '';
    private string $origin;

    /**
     * Named constructor for invalid php date strings
     */
    public static function forInvalidDateString(string $date, Exception $previous): ToDoRuntimeException
    {
        $message  = sprintf('Date Format: %s is invalid', $date);
        $instance = new static($message, 0, $previous);

        $instance->userNotification = "Only dates in the format Y-m-d are allowed";
        $instance->origin           = 'ScheduledDate';

        return $instance;
    }

    /**
     * Named constructor for invalid time strings
     */
    public static function forInvalidTimeString(string $time): ToDoRuntimeException
    {
        $message  = sprintf('Time format: %s is invalid', $time);
        $instance = new static($message);

        $instance->userNotification = 'Only time in the format hours:minutes are allowed';
        $instance->origin           = 'scheduledTime';

        return $instance;
    }

    /**
     * Named constructor for past schedule dates
     */
    public static function forDatesInPast(ScheduledDate $scheduledDate): ToDoRuntimeException
    {
        $message  = sprintf('Date is past: %s ', (string) $scheduledDate);
        $instance = new static($message);

        $instance->userNotification = 'The schedule date is in the past please enter a date that is in the future';
        $instance->origin           = 'ScheduledDate';

        return $instance;
    }

    /**
     * Named constructor for max number of tasks exceeded
     */
    public static function forMaximumNumberOfTasks(int $maxTasks, ScheduledTask $scheduledTask): ToDoRuntimeException
    {
        $message  = sprintf('Max amount of %s tasks exceeded', $maxTasks);
        $instance = new static($message);

        $instance->userNotification = sprintf("It's not allowed to add more than %s tasks", $maxTasks);
        $instance->origin           =  strval($scheduledTask->getTaskIdentifier()) . '.ScheduledTask.';

        return $instance;
    }

    /**
     * Named constructor for duplicate scheduled task times
     */
    public static function forDuplicateScheduledTaskTimes(ScheduledTime $scheduledTime, string $identifier): ToDoRuntimeException
    {
        $message  = sprintf('Duplicate task time: %s', (string) $scheduledTime);
        $instance = new static($message);

        $instance->userNotification = sprintf(
            "A tasks with this time: %s is already scheduled",
            (string) $scheduledTime
        );

        $instance->origin = $identifier . '.ScheduledTime';

        return $instance;
    }

    /**
     * Named constructor for ConstraintViolationList
     */
    public static function fromConstraintViolationList(ConstraintViolationListInterface $violationList): self {
        $violation = $violationList->get(1);

        $instance = new static($violation->getMessage());
        $instance->userNotification = $violation->getMessage();
        $instance->origin = $violation->getPropertyPath();

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function getUserNotification(): string
    {
        return $this->userNotification;
    }

    /**
     * @inheritDoc
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }
}
