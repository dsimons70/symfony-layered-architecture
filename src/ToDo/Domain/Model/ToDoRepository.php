<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use App\User\Domain\Model\UserIdentifier;
use RuntimeException;

interface ToDoRepository
{
    /**
     * Find all ToDo's for the given name
     * @throws RuntimeException
     */
    public function findByUserIdentifier(UserIdentifier $userIdentifier): ToDoList;

    /**
     * Find by unique identifier
     * @throws RuntimeException
     */
    public function findByIdentifier(ToDoIdentifier $toDoIdentifier): ?ToDo;

    /**
     * Add ToDoEntry
     * @throws RuntimeException
     */
    public function add(ToDo $toDo): void;

    /**
     * Replace existing ToDo-Entity
     * @throws RuntimeException
     */
    public function replace(ToDo $toDo): void;

    /**
     * Remove existing ToDo-Entity
     * @throws RuntimeException
     */
    public function remove(ToDo $toDo): void;

    /**
     * Generate unique identifier
     * @throws RuntimeException
     */
    public function nextToDoIdentifier(): ToDoIdentifier;

    /**
     * Generate unique identifier
     * @throws RuntimeException
     */
    public function nextTaskIdentifier(): TaskIdentifier;
}
