<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

/**
 * Entity for a scheduled task
 */
class ScheduledTask
{
    private TaskIdentifier $taskIdentifier;
    private ScheduledTime $scheduledTime;
    private string $description;
    private bool $isDone = false;

    private function __construct(TaskIdentifier $taskIdentifier, ScheduledTime $scheduledTime, string $description)
    {
        $this->taskIdentifier = $taskIdentifier;
        $this->scheduledTime  = $scheduledTime;
        $this->description    = $description;
    }

    public static function schedule(
        TaskIdentifier $taskIdentifier,
        ScheduledTime $scheduledTime,
        string $description
    ): ScheduledTask {
        return new self($taskIdentifier, $scheduledTime, $description);
    }

    /**
     * Getter for TaskIdentifier
     */
    public function getTaskIdentifier(): TaskIdentifier
    {
        return $this->taskIdentifier;
    }

    /**
     * Getter for ScheduledTime
     */
    public function getScheduledTime(): ScheduledTime
    {
        return $this->scheduledTime;
    }

    /**
     * Getter for the description string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Mark task as done
     */
    public function markAsDone()
    {
        $this->isDone = true;
    }

    /**
     * Check if the task has been marked as done
     */
    public function isDone(): bool
    {
        return $this->isDone;
    }
}
