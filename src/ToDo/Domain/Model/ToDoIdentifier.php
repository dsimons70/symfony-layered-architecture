<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use App\Shared\Domain\Model\Identifier;

class ToDoIdentifier extends Identifier
{
    /**
     * @inheritDoc
     */
    public static function fromString(string $identifier): self
    {
        return new static($identifier);
    }
}
