<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\User\Domain\Model\UserIdentifier;
use ArrayIterator;
use Countable;
use IteratorAggregate;

/**
 * Root aggregate for "todo" entities
 */
class ToDo implements IteratorAggregate, Countable
{
    public const MAX_ALLOWED_TASKS = 5;

    private ToDoIdentifier $toDoIdentifier;
    private UserIdentifier $userIdentifier;
    private ScheduledDate $scheduledDate;

    /** @var ScheduledTask[] */
    private array $scheduledTasks = [];

    private function __construct(
        ToDoIdentifier $toDoIdentifier,
        UserIdentifier $userIdentifier,
        ScheduledDate $scheduledDate
    ) {
        $this->toDoIdentifier = $toDoIdentifier;
        $this->userIdentifier = $userIdentifier;
        $this->scheduledDate  = $scheduledDate;
    }

    /**
     * Named constructor to schedule a "todo" for the given userIdentifier
     */
    public static function scheduleFor(
        UserIdentifier $userIdentifier,
        ScheduledDate $scheduledDate,
        ToDoIdentifier $toDoIdentifier
    ): ToDo {
        return new self($toDoIdentifier, $userIdentifier, $scheduledDate);
    }

    /**
     * Schedule a task
     * @throws ToDoRuntimeException
     */
    public function scheduleTask(ScheduledTask $scheduledTask)
    {
        $identifier = (string) $scheduledTask->getTaskIdentifier();

        if ($this->count() >= self::MAX_ALLOWED_TASKS) {
            throw ToDoRuntimeException::forMaximumNumberOfTasks(self::MAX_ALLOWED_TASKS, $scheduledTask);
        }

        if ($this->isTimeAlreadyScheduled($scheduledTask->getScheduledTime())) {
            throw ToDoRuntimeException::forDuplicateScheduledTaskTimes($scheduledTask->getScheduledTime(), $identifier);
        }

        $this->scheduledTasks[$identifier] = $scheduledTask;
        $this->sortTasksByTime(); // Ensure correct sort to avoid sort in persistence layer per request
    }

    /**
     * Returns all unprocessed task
     * @return ScheduledTask[]
     */
    public function getUndoneTasks(): array
    {
        $undoneTasks = [];

        foreach ($this->scheduledTasks as $scheduledTask) {
            if (!$scheduledTask->isDone()) {
                $undoneTasks[] = $scheduledTask;
            }
        }

        return $undoneTasks;
    }

    /**
     * Getter for identifier
     */
    public function getIdentifier(): ToDoIdentifier
    {
        return $this->toDoIdentifier;
    }

    /**
     * Getter for userIdentifier
     */
    public function getUserIdentifier(): UserIdentifier
    {
        return $this->userIdentifier;
    }

    /**
     * Getter for schedule date
     */
    public function getScheduledDate(): ScheduledDate
    {
        return $this->scheduledDate;
    }

    /**
     * Getter for all scheduled task
     * @return ScheduledTask[]
     */
    public function getScheduledTasks(): array
    {
        return $this->scheduledTasks;
    }

    /**
     * Checks if all scheduled tasks are done
     */
    public function allTasksDone(): bool
    {
        foreach ($this->scheduledTasks as $task) {
            if (!$task->isDone()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->scheduledTasks);
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return count($this->scheduledTasks);
    }

    /**
     * Sort function to sort the tasks ascending by time
     */
    private function sortTasksByTime(): void
    {
        uasort($this->scheduledTasks, function (ScheduledTask $taskA, ScheduledTask $taskB) {
            $timeA = $taskA->getScheduledTime();
            $timeB = $taskB->getScheduledTime();

            return $timeB->combinedTimeComparison($timeA);
        });
    }

    /**
     * Checks for task with duplicate times
     */
    private function isTimeAlreadyScheduled(ScheduledTime $scheduledTime): bool
    {
        foreach ($this->scheduledTasks as $task) {
            if ($task->getScheduledTime()->equals($scheduledTime)) {
                return true;
            }
        }

        return false;
    }
}
