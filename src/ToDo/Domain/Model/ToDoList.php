<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;

/**
 * Collection class for ToDos
 */
class ToDoList implements IteratorAggregate, Countable
{
    /** @var ToDo[] */
    private array $toDoList = [];

    /**
     * Named constructor to create an instance based on the given ToDoEntry[] array
     * @param ToDo[]
     * @return ToDoList
     */
    public static function fromArray(array $toDos): ToDoList
    {
        $toDoList = new self();

        foreach ($toDos as $toDo) {
            $toDoList->addToDo($toDo);
        }

        return $toDoList;
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->toDoList);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->toDoList);
    }

    /**
     * Function to ensure type safety
     */
    private function addToDo(ToDo $toDo)
    {
        $this->toDoList[$toDo->getIdentifier()->__toString()] = $toDo;
    }
}
