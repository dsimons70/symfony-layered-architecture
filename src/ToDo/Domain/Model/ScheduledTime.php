<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;

/**
 * Value Object for schedule times like 16:15 (hours:minutes)
 */
class ScheduledTime
{
    private int $hour;
    private int $minute;

    private function __construct(int $hour, int $minute)
    {
        $this->hour   = $hour;
        $this->minute = $minute;
    }

    /**
     * Named Constructor to create an instance based on the given time string (hh:mm)
     * @throws ToDoRuntimeException
     */
    public static function fromString(string $time): ScheduledTime
    {
        if (preg_match("/^(\d{1,2}):(\d{2})$/", $time, $matches) !== 1) {
            throw ToDoRuntimeException::forInvalidTimeString($time);
        }

        $hour   = (int) $matches[1];
        $minute = (int) $matches[2];

        if ($hour < 0 || $hour > 23 || $minute < 0 || $minute > 59) {
            throw ToDoRuntimeException::forInvalidTimeString($time);
        }

        return new self($hour, $minute);
    }

    /**
     * Compares the time with the <=> operator
     *
     *  returns  0 if both times are equal
     *  returns -1 if given time is earlier
     *  returns  1 if given time is later
     */
    public function combinedTimeComparison(ScheduledTime $scheduledTime): int
    {
        $minutesA = ($scheduledTime->getHour() * 60) + $scheduledTime->getMinute();
        $minutesB = ($this->getHour() * 60) + $this->getMinute();

        return $minutesA <=> $minutesB;
    }

    /**
     * Equal comparison of both times
     */
    public function equals(ScheduledTime $scheduledTime): bool
    {
        return $this->hour === $scheduledTime->getHour() && $this->minute === $scheduledTime->getMinute();
    }

    /**
     * Returns the hour (0-23)
     */
    public function getHour(): int
    {
        return $this->hour;
    }

    /**
     * Returns the minute (0-59)
     */
    public function getMinute(): int
    {
        return $this->minute;
    }

    /**
     * Returns the formatted string representation (hh:mm)
     */
    public function __toString(): string
    {
        return sprintf("%02d:%02d", $this->hour, $this->minute);
    }
}
