<?php

declare(strict_types=1);

namespace App\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use DateTime;
use DateTimeImmutable;
use Exception;

/**
 * Valued object for a schedule date
 */
class ScheduledDate
{
    private DateTimeImmutable $dateTime;

    private function __construct(DateTimeImmutable $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * Named constructor to crate an instance based on the given PHP date string
     * @see https://www.php.net/manual/de/datetime.formats.date.php
     * @throws ToDoRuntimeException
     */
    public static function fromString(string $date): ScheduledDate
    {
        try {
            $dateTime = new DateTimeImmutable($date);
        } catch (Exception $exception) {
            throw ToDoRuntimeException::forInvalidDateString($date, $exception);
        }

        return new self($dateTime);
    }

    /**
     * Return the date as a string in the format Y-m-d
     */
    public function __toString(): string
    {
        return $this->dateTime->format("Y-m-d");
    }

    /**
     * Transform to a timestamp integer value
     */
    public function toTimestamp(): int
    {
        return $this->dateTime->getTimestamp();
    }

    /**
     * Checks it the date is in the past
     */
    public function isInThePast(): bool
    {
        $nowDate = new DateTime('now');
        $nowDate->setTime(0, 0);

        return $this->dateTime < $nowDate;
    }
}
