# Case-Study: todo-app with layered architecture

This application is a simple example Symfony 4 application for to-do lists. Instead to use the 
standard Symfony structure we use a "layered architecture"approach as it was descriped by David 
Evans book about "Domain Driven Design". You will find several similar approaches under the search 
terms "Port and Adapters", "Onion architecture" or  "Hexagonal architecture".

![todo-app_thump.gif](./public/images/todo-app_thump.gif)

There was a great presentation from Matthias Noback at the Symfony Live conference 2018 in Berlin 
about this topic: [Advanced Application Architecture](https://de.slideshare.net/matthiasnoback/advanced-application-architecture-symfony-live-berlin-2018) 
and on his personal [blog](https://matthiasnoback.nl/2017/08/layers-ports-and-adapters-part-2-layers/). 
Another article in german is from the Microsoft tech blog: 
[Erstellen eines DDD-orientierten Microservices](https://docs.microsoft.com/de-de/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice). 

## Installation

If you have Docker installed, its easy to build this example:

```bash
git clone https://gitlab.com/dsimons70/symfony-layered-architecture.git
cd symfony-layered-architecture
docker-compose build
docker-compose up -d 
```
Login into the php container and use composer install: 

```bash
docker exec -it symfony-layered-architecture_php_1 /bin/sh
cd /todo-app/
composer install
```
Now you can start your browser an run the app under [http://localhost](http://localhost). The Redis and the MySql 
Databases are generated at the first start of both containers.

## Getting Started

If you select the MySql database as an storage engine you can use the test user

```yaml
Username: john.doe
Password: geheim
```

## Coding and architecture

This is not production ready code its just a case study about interesting approaches pattern and 
software architecture in common. In addition to the good documentation it one of Symfony's biggest 
strength don't to force a developer into a specific direction. Nevertheless symfony standard 
structure tempt the developer to go in the wrong direction, I have seen controllers with more than 
2000 lines of spaghetti code,  mixed control, view and business logic hidden in hundreds of 
form handlers, form events, doctrine events or symfony events. 

### The framework (vendor) lock-in
Even before Symfony 1 exists, I have created applications in three levels, as described by Martin 
Fowler in this article: [Service Layer](https://martinfowler.com/eaaCatalog/serviceLayer.html) 
with great success. The result was relatively clean code and thin controllers. It was easy to 
reuse the entire application behavior of the service layer in another controller to provide 
a soap API for a white label partner.

However, most frameworks will still creep into the application code, and you are forever 
tied to a framework that may one day become obsolete or become unmaintained. The layered
architecture approach described in David Evans blue book about domain driven design is 
designed to separate your application from framework or vendor and library code.

### Doctrine and symfony forms are not the best solution for every project
If you build internal admin tools with mostly CRUD operations doctrine and the sonata admin 
is the way to go. The problem with ORM is you will end up with huge aggregated object trees
with bloodless anemic donain models and tons of public setter methods, there is a very good 
article from Martin Fowler about this topic: 
[Anemic Domain Model](https://martinfowler.com/bliki/AnemicDomainModel.html)

You model with all the public setters will be changed in hidden doctrine events, in form 
events or in several symfony listener in a dependent flow order and your journey into 
hard to debug code begins. Your application logic which should belong together getting 
spread around in fat controllers, form handlers and symfony and form listeners or doctrine 
events and become hard to understand specifically for new developers who don't know all 
your fancy dirty hacks.  

### Split by bounded contexts first!
 
Don't slice your hole application into layers, if you use the layered architecture it's important 
to split your business domain in vertical sliced bounded contexts. Here is an example for a simple 
shop system:

* ProductInventory (MySqL; Memcached)
    * Application
    * Domain
    * Infrastructure
* ProductSearch (Elastic search or Solr/Lucene)
    * Application
    * Domain
    * Infrastructure
* ProductCart (Redis)
    * Application
    * Domain
    * Infrastructure
* Order (Redis)
    * Application
    * Domain
    * Infrastructure
* Invoice (MySql)
    * Application
    * Domain
    * Infrastructure
* Delivery (MySql)
    * Application
    * Domain
    * Infrastructure    
* Identity (OAuth2 Server)
    * Application
    * Domain
    * Infrastructure

Now you have split your big evil monolith into vertical sliced contexts and namespaces, I would recommend to use for each 
context a own data storage which best suits its needs. Thanks to the layered architecture you can change your persistence 
strategy at runtime. 

By the way is this structure not a good starting point to implement each context as a "Mircroservice"?
Nevertheless be careful, a huge amount of Microservices which depend on each other can become even a bigger nightmare as 
your 'evil' monolith.   


