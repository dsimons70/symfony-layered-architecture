<?php

declare(strict_types=1);

namespace App\Tests\User\Domain\Model;

use App\User\Domain\Model\UserRoles;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\User\Domain\Model\UserRoles
 */
class UserRolesTest extends TestCase
{
    /**
     * @test
     */
    public function forStandardUser_SetsStandardUserRoles_ReturnsRoles()
    {
        $expectedRoles = [UserRoles::STANDARD_USER];

        $actual = UserRoles::forStandardUser();

        self::assertSame($expectedRoles, $actual->toArray());
        self::assertSame($expectedRoles, $actual->getIterator()->getArrayCopy());
    }
}
