<?php

declare(strict_types=1);

namespace App\Tests\User\Domain\Model;

use App\User\Domain\Model\UserIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\User\Domain\Model\UserIdentifier
 */
class UserIdentifierTest extends TestCase
{
    /**
     * @test
     */
    public function fromString_SetsValue_ReturnsSameValue()
    {
        $expectedIdentifier = uniqid('user:');
        $userIdentifier     = UserIdentifier::fromString($expectedIdentifier);

        $actual = (string) $userIdentifier;

        self::assertSame($expectedIdentifier, $actual);
    }
}
