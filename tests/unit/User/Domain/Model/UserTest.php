<?php

declare(strict_types=1);

namespace App\Tests\User\Domain\Model;

use App\User\Domain\Model\User;
use App\User\Domain\Model\UserCredentials;
use App\User\Domain\Model\UserIdentifier;
use App\User\Domain\Model\UserRoles;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\User\Domain\Model\User
 */
class UserTest extends TestCase
{
    /**
     * @test
     */
    public function asStandardUser_SetsValues_ReturnsSameValues(): void
    {
        $expectedRoles       = UserRoles::forStandardUser();
        $expectedIdentifier  = UserIdentifier::fromString(uniqid('user:'));
        $expectedCredentials = UserCredentials::fromEncodedPassword(
            '$argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU',
            'john.doe'
        );

        $actual = User::asStandardUser($expectedIdentifier, $expectedCredentials);

        self::assertSame($expectedIdentifier, $actual->getIdentifier());
        self::assertSame($expectedCredentials, $actual->getCredentials());
        self::assertEquals($expectedRoles, $actual->getRoles());
    }
}
