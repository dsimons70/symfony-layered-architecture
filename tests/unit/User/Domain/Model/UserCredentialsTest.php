<?php

declare(strict_types=1);

namespace App\Tests\User\Domain\Model;

use App\User\Domain\Model\UserCredentials;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\User\Domain\Model\UserCredentials
 */
class UserCredentialsTest extends TestCase
{
    /**
     * @test
     */
    public function fromEncodedPassword_SetsValues_ReturnsSameValues(): void
    {
        $expectedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';
        $expectedUser     = 'john.doe';

        $actual = UserCredentials::fromEncodedPassword($expectedPassword, $expectedUser);

        self::assertSame($expectedPassword, $actual->getPassword());
        self::assertSame($expectedUser, $actual->getUsername());
    }
}
