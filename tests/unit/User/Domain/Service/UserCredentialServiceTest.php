<?php

declare(strict_types=1);

namespace App\Tests\User\Domain\Service;

use App\User\Domain\Exception\UserRuntimeException;
use App\User\Domain\Model\UserCredentials;
use App\User\Domain\Service\PasswordHashing;
use App\User\Domain\Service\UserCredentialService;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * @covers \App\User\Domain\Service\UserCredentialService
 * @covers \App\User\Domain\Exception\UserRuntimeException
 */
class UserCredentialServiceTest extends TestCase
{
    /** @var PasswordHashing */
    private $passwordHashing;

    /** @var UserCredentialService */
    private $userCredentialService;

    public function setUp(): void
    {
        $this->passwordHashing       = $this->prophesize(PasswordHashing::class);
        $this->userCredentialService = new UserCredentialService($this->passwordHashing->reveal());
    }

    /**
     * @test
     */
    public function encode_EncodesPassword_ReturnsCredentials(): void
    {
        $expectedUsername        = 'john.doe';
        $expectedRawPassword     = 'top_secret';
        $expectedEncodedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';

        $this->passwordHashing->hashPassword($expectedRawPassword)
            ->willReturn($expectedEncodedPassword)
            ->shouldBeCalled();

        $actual = $this->userCredentialService->encode($expectedUsername, $expectedRawPassword);

        self::assertSame($expectedUsername, $actual->getUsername());
        self::assertSame($expectedEncodedPassword, $actual->getPassword());
    }

    /**
     * @test
     */
    public function encode_CatchesException_ThrowsUserRuntimeException(): void
    {
        $expectedUsername    = 'john.doe';
        $expectedRawPassword = 'top_secret';

        $this->passwordHashing->hashPassword($expectedRawPassword)->willThrow(BadCredentialsException::class);
        $this->expectException(UserRuntimeException::class);

        $this->userCredentialService->encode($expectedUsername, $expectedRawPassword);
    }

    /**
     * @test
     */
    public function isValid_CanNotVerifyPassword_ReturnsFalse(): void
    {
        $expectedUsername        = 'john.doe';
        $expectedRawPassword     = 'top_secret';
        $expectedEncodedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';

        $credentials = UserCredentials::fromEncodedPassword($expectedEncodedPassword, $expectedUsername);

        $this->passwordHashing->verifyPassword($credentials->getPassword(), $expectedRawPassword)
            ->willReturn(false)
            ->shouldBeCalled();

        $actual = $this->userCredentialService->isValid($credentials, $expectedUsername, $expectedRawPassword);

        self::assertFalse($actual);
    }

    /**
     * @test
     */
    public function isValid_CatchesException_ReturnsFalse(): void
    {
        $expectedUsername        = 'john.doe';
        $expectedRawPassword     = 'top_secret';
        $expectedEncodedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';

        $credentials = UserCredentials::fromEncodedPassword($expectedEncodedPassword, $expectedUsername);

        $this->passwordHashing->verifyPassword(
            $credentials->getPassword(),
            $expectedRawPassword
        )->willThrow(InvalidArgumentException::class);

        $actual = $this->userCredentialService->isValid($credentials, $expectedUsername, $expectedRawPassword);

        self::assertFalse($actual);
    }

    /**
     * @test
     */
    public function isValid_UserNameDoesNotMatch_ReturnsFalse(): void
    {
        $expectedUsername        = 'john.doe';
        $expectedRawPassword     = 'top_secret';
        $expectedEncodedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';

        $credentials = UserCredentials::fromEncodedPassword($expectedEncodedPassword, $expectedUsername);

        $this->passwordHashing->verifyPassword($credentials->getPassword(), $expectedRawPassword)
            ->willReturn(true)
            ->shouldBeCalled();

        $actual = $this->userCredentialService->isValid($credentials, 'wrong.username', $expectedRawPassword);

        self::assertFalse($actual);
    }

    /**
     * @test
     */
    public function isValid_UserNameMatches_ReturnsTrue(): void
    {
        $expectedUsername        = 'john.doe';
        $expectedRawPassword     = 'top_secret';
        $expectedEncodedPassword = 'argon2id$v=19$m=65536,t=4,p=1$x4JMHmOk27+Dpe1QaX5+4A$baS1nVhpdkzEkyv5NEKBl2BpC6vbDbjhC56HG4oV0YU';

        $credentials = UserCredentials::fromEncodedPassword($expectedEncodedPassword, $expectedUsername);

        $this->passwordHashing->verifyPassword($credentials->getPassword(), $expectedRawPassword)
            ->willReturn(true)
            ->shouldBeCalled();

        $actual = $this->userCredentialService->isValid($credentials, $expectedUsername, $expectedRawPassword);

        self::assertTrue($actual);
    }
}
