<?php

declare(strict_types=1);

namespace App\Tests\User\Application\Login;

use App\Shared\Infrastructure\Persistence\MySql\MySqlPersistenceType;
use App\Shared\Infrastructure\Persistence\PersistenceFactory;
use App\Shared\Infrastructure\Persistence\Redis\RedisPersistenceType;
use App\User\Application\Login\LoginDto;
use App\User\Application\Login\UserLoginService;
use App\User\Domain\Model\User;
use App\User\Domain\Model\UserCredentials;
use App\User\Domain\Model\UserRepository;
use App\User\Domain\Service\UserCredentialService;
use App\User\Infrastructure\Session\SessionRegistry;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use RuntimeException;

/**
 * @covers \App\User\Application\Login\UserLoginService
 * @covers \App\User\Application\Login\LoginDto
 */
class UserLoginServiceTest extends TestCase
{
    /** @var SessionRegistry */
    private $sessionRegistry;

    /** @var PersistenceFactory */
    private $persistenceFactory;

    /** @var UserRepository */
    private $userRepository;

    /** @var UserCredentialService */
    private $userCredentialsService;

    /** @var UserLoginService */
    private $userLoginService;

    /** @var $loginUser */
    private $loginUser;

    /** @var User */
    private $user;

    public function setUp(): void
    {
        $this->sessionRegistry        = $this->prophesize(SessionRegistry::class);
        $this->persistenceFactory     = $this->prophesize(PersistenceFactory::class);
        $this->userRepository         = $this->prophesize(UserRepository::class);
        $this->userCredentialsService = $this->prophesize(UserCredentialService::class);

        $this->userLoginService = new UserLoginService(
            $this->sessionRegistry->reveal(),
            $this->persistenceFactory->reveal(),
            $this->userRepository->reveal(),
            $this->userCredentialsService->reveal()
        );

        $this->loginUser = new LoginDto('john.doe', 'top_secret_hash', 'type_redis');
        $this->user      = $this->prophesize(User::class);
    }

    /**
     * @test
     */
    public function checkLogin_UserNotExists_ReturnsNull(): void
    {
        $this->userRepository->findByUsername($this->loginUser->getUsername())->willReturn(null);

        $actual = $this->userLoginService->checkLogin($this->loginUser);

        self::assertNull($actual);
    }

    /**
     * @test
     */
    public function checkLogin_InvalidCredentials_ReturnsNull(): void
    {
        $expectedCredentials = $this->prophesize(UserCredentials::class)->reveal();

        $this->userRepository->findByUsername($this->loginUser->getUsername())->willReturn($this->user);
        $this->user->getCredentials()->willReturn($expectedCredentials);

        $this->userCredentialsService->isValid(
            $expectedCredentials,
            $this->loginUser->getUsername(),
            $this->loginUser->getPassword()
        )->shouldBeCalled()->willReturn(false);

        $actual = $this->userLoginService->checkLogin($this->loginUser);

        self::assertNull($actual);
    }

    /**
     * @test
     */
    public function checkLogin_PersistenceTypeNotRegistered_ThrowsException(): void
    {
        $this->expectException(RuntimeException::class);

        $this->userRepository->findByUsername($this->loginUser->getUsername())->willReturn($this->user);
        $this->user->getCredentials()->willReturn($this->prophesize(UserCredentials::class)->reveal());
        $this->userCredentialsService->isValid(Argument::cetera())->shouldBeCalled()->willReturn(true);
        $this->persistenceFactory->isPersistenceTypRegistered($this->loginUser->getPersistenceTypeIdentifier())->willReturn(false);

        $this->userLoginService->checkLogin($this->loginUser);
    }

    /**
     * @test
     */
    public function checkLogin_ValidCredentials_SetsUserSessionReturnsUser(): void
    {
        $expectedCredentials = $this->prophesize(UserCredentials::class);

        $this->userRepository->findByUsername($this->loginUser->getUsername())->willReturn($this->user);
        $this->user->getCredentials()->willReturn($expectedCredentials);
        $this->userCredentialsService->isValid(Argument::cetera())->shouldBeCalled()->willReturn(true);
        $this->persistenceFactory->isPersistenceTypRegistered($this->loginUser->getPersistenceTypeIdentifier())->willReturn(true);

        $this->sessionRegistry->setPersistenceTypeIdentifier($this->loginUser->getPersistenceTypeIdentifier())->shouldBeCalled();
        $this->sessionRegistry->setUser($this->user)->shouldBeCalled();

        $actual = $this->userLoginService->checkLogin($this->loginUser);

        self::assertSame($this->user->reveal(), $actual);
    }

    /**
     * @test
     */
    public function getUser_ReturnsFoundUser(): void
    {
        $this->userRepository->findByUsername($this->loginUser->getUsername())->willReturn($this->user);

        $actual = $this->userLoginService->getUser($this->loginUser->getUsername());

        self::assertSame($this->user->reveal(), $actual);
    }

    /**
     * @test
     */
    public function getRegisteredPersistenceTypes_ReturnsAllFoundTypes(): void
    {
        $expectedArray = [new MySqlPersistenceType(), new RedisPersistenceType()];
        $this->persistenceFactory->getAllPersistenceTypes()->willReturn($expectedArray);

        $actual = $this->userLoginService->getRegisteredPersistenceTypes();

        self::assertSame($expectedArray, $actual);
    }
}
