<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Edit;

use App\ToDo\Application\Edit\EditTaskDto;
use App\ToDo\Application\Edit\ToDoEditService;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * @covers \App\ToDo\Application\Edit\ToDoEditService
 * @covers \App\ToDo\Application\Edit\EditDto
 * @covers \App\ToDo\Application\Edit\EditTaskDto
 */
class ToDoEditServiceTest extends TestCase
{
    /** @var ToDoRepository */
    private $toDoRepository;

    /** @var ToDoEditService */
    private $toDoEditService;

    public function setUp(): void
    {
        $this->toDoRepository  = $this->prophesize(ToDoRepository::class);
        $this->toDoEditService = new ToDoEditService($this->toDoRepository->reveal());
    }

    /**
     * @test
     * Edit, Creates ToDo, Calls Replace
     * testEditCreatesToDoCallsReplace
     */
    public function edit_CreatesToDo_CallsRepositoryReplace(): void
    {
        $editTaskDone   = ToDoEditFactory::createTask(true);
        $editTaskUndone = ToDoEditFactory::createTask(false);

        $editTasks = [
            $editTaskDone->getTaskIdentifier()   => $editTaskDone,
            $editTaskUndone->getTaskIdentifier() => $editTaskUndone,
        ];

        $editToDo = ToDoEditFactory::createEditToDo($editTasks);

        $this->toDoRepository->replace(Argument::that(function (ToDo $actualToDo) use ($editToDo, $editTasks) {
            self::assertSame($editToDo->getScheduledDate(), $actualToDo->getScheduledDate()->__toString());
            self::assertSame($editToDo->getToDoIdentifier(), $actualToDo->getIdentifier()->__toString());
            self::assertSame($editToDo->getUserIdentifier(), $actualToDo->getUserIdentifier()->__toString());

            foreach ($actualToDo->getScheduledTasks() as $actualTask) {
                /** @var EditTaskDto $editTask */
                $editTask = $editTasks[strval($actualTask->getTaskIdentifier())];

                self::assertSame($editTask->getTaskIdentifier(), $actualTask->getTaskIdentifier()->__toString());
                self::assertSame($editTask->getScheduledTime(), $actualTask->getScheduledTime()->__toString());
                self::assertSame($editTask->getDescription(), $actualTask->getDescription());
                self::assertSame($editTask->isDone(), $actualTask->isDone());
            }

            return $actualToDo;
        }))->shouldBeCalled();

        $this->toDoEditService->edit($editToDo);
    }

    /**
     * @test
     */
    public function retrieveItem_CallsRepository_ReturnsSameMatchingObject(): void
    {
        $identifier         = uniqid('todo:');
        $expectedIdentifier = ToDoIdentifier::fromString($identifier);
        $expectedToDo       = $this->prophesize(ToDo::class)->reveal();

        $this->toDoRepository->findByIdentifier($expectedIdentifier)->willReturn($expectedToDo)->shouldBeCalled();

        $actual = $this->toDoEditService->getToDo($identifier);

        self::assertSame($expectedToDo, $actual);
    }

    /**
     * @test
     */
    public function nextTaskIdentifier_CallsRepository_ReturnsSameIdentifier(): void
    {
        $expectedIdentifier = $this->prophesize(TaskIdentifier::class)->reveal();
        $this->toDoRepository->nextTaskIdentifier()->shouldBeCalled()->willReturn($expectedIdentifier);

        $actual = $this->toDoEditService->nextTaskIdentifier();

        self::assertSame($expectedIdentifier, $actual);
    }

}
