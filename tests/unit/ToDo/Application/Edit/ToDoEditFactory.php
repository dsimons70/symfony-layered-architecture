<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Edit;

use App\ToDo\Application\Edit\EditDto;
use App\ToDo\Application\Edit\EditTaskDto;

class ToDoEditFactory
{
    public static function createTask(bool $isDone): EditTaskDto
    {
        $identifier = uniqid('task:');
        $randomTime = sprintf('%02s:%02s', rand(0, 23), rand(0, 59));

        return new EditTaskDto(
            $identifier,
            $randomTime,
            'Description for task:' . $identifier,
            $isDone
        );
    }

    public static function createEditToDo(array $editTasks): EditDto
    {
        return new EditDto(
            uniqid('ToDo:'),
            uniqid('User:'),
            date("Y-m-d"),
            $editTasks
        );
    }
}
