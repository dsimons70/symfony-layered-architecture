<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Schedule;

use App\ToDo\Application\Schedule\ScheduleDto;
use App\ToDo\Application\Schedule\ScheduleTaskDto;
use DateTime;

class ToDoScheduleFactory
{
    public static function createScheduleTask(string $time): ScheduleTaskDto
    {
        $identifier = uniqid('task:');

        return new ScheduleTaskDto(
            $identifier,
            $time,
            'Description for task:' . $identifier
        );
    }

    public static function createScheduleToDo(array $scheduledTasks, string $date = 'now'): ScheduleDto
    {
        $dateTime = new DateTime($date);

        return new ScheduleDto(
            uniqid('ToDo:'),
            uniqid('User:'),
            $dateTime->format("Y-m-d"),
            $scheduledTasks
        );
    }
}
