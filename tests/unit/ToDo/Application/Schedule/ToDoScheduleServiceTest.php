<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Schedule;

use App\ToDo\Application\Schedule\ToDoScheduleService;
use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * @covers \App\ToDo\Application\Schedule\ToDoScheduleService
 * @covers \App\ToDo\Application\Schedule\ScheduleTaskDto
 * @covers \App\ToDo\Application\Schedule\ScheduleDto
 * @covers \App\ToDo\Domain\Exception\ToDoRuntimeException
 */
class ToDoScheduleServiceTest extends TestCase
{
    /** @var ToDoRepository */
    private $toDoRepository;

    /** @var ToDoScheduleService */
    private $toDoScheduleService;

    public function setUp(): void
    {
        $this->toDoRepository      = $this->prophesize(ToDoRepository::class);
        $this->toDoScheduleService = new ToDoScheduleService($this->toDoRepository->reveal());

    }

    /**
     * @test
     */
    public function schedule_CreatesToDo_CallsRepositoryAdd(): void
    {
        $scheduleTaskOne   = ToDoScheduleFactory::createScheduleTask('15:00');
        $scheduleTaskTwo   = ToDoScheduleFactory::createScheduleTask('18:30');
        $scheduleTaskThree = ToDoScheduleFactory::createScheduleTask('22:15');

        $scheduledTasks = [
            $scheduleTaskOne->getTaskIdentifier()   => $scheduleTaskOne,
            $scheduleTaskTwo->getTaskIdentifier()   => $scheduleTaskTwo,
            $scheduleTaskThree->getTaskIdentifier() => $scheduleTaskThree,
        ];

        $scheduleToDo = ToDoScheduleFactory::createScheduleToDo($scheduledTasks);

        $this->toDoRepository->add(Argument::that(function (ToDo $actualToDo) use ($scheduleToDo, $scheduledTasks) {
            self::assertSame($scheduleToDo->getToDoIdentifier(), $actualToDo->getIdentifier()->__toString());
            self::assertSame($scheduleToDo->getScheduledDate(), $actualToDo->getScheduledDate()->__toString());

            foreach ($actualToDo->getScheduledTasks() as $scheduledTask) {
                $expectedTask = $scheduledTasks[strval($scheduledTask->getTaskIdentifier())];

                self::assertSame($expectedTask->getTaskIdentifier(), $scheduledTask->getTaskIdentifier()->__toString());
                self::assertSame($expectedTask->getScheduledTime(), $scheduledTask->getScheduledTime()->__toString());
                self::assertSame($expectedTask->getDescription(), $scheduledTask->getDescription());
            }
            return $actualToDo;

        }))->shouldBeCalled();

        $actual = $this->toDoScheduleService->schedule($scheduleToDo);

        self::assertInstanceOf(ToDo::class, $actual);
        self::assertCount(count($scheduledTasks), $actual);
    }

    /**
     * @test
     */
    public function schedule_DateIsInPast_ThrowsException(): void
    {
        $this->expectException(ToDoRuntimeException::class);
        $scheduleToDo = ToDoScheduleFactory::createScheduleToDo([], 'yesterday');

        $this->toDoScheduleService->schedule($scheduleToDo);
    }

    /**
     * @test
     */
    public function nextToDoIdentifier_CallsRepository_ReturnsSameIdentifier(): void
    {
        $expectedIdentifier = $this->prophesize(ToDoIdentifier::class)->reveal();
        $this->toDoRepository->nextToDoIdentifier()->shouldBeCalled()->willReturn($expectedIdentifier);

        $actual = $this->toDoScheduleService->nextToDoIdentifier();

        self::assertSame($expectedIdentifier, $actual);
    }

    /**
     * @test
     */
    public function nextTaskIdentifier_CallsRepository_ReturnsSameIdentifier(): void
    {
        $expectedIdentifier = $this->prophesize(TaskIdentifier::class)->reveal();
        $this->toDoRepository->nextTaskIdentifier()->shouldBeCalled()->willReturn($expectedIdentifier);

        $actual = $this->toDoScheduleService->nextTaskIdentifier();

        self::assertSame($expectedIdentifier, $actual);
    }
}
