<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Overview;

use App\ToDo\Application\Overview\OverviewDto;
use App\ToDo\Application\Overview\ToDoOverviewService;
use App\ToDo\Domain\Model\ToDoList;
use App\ToDo\Domain\Model\ToDoRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * @covers \App\ToDo\Application\Overview\OverviewDto
 * @covers \App\ToDo\Application\Overview\ToDoOverviewService
 */
class ToDoOverviewServiceTest extends TestCase
{
    /** @var ToDoRepository */
    private $toDoRepository;

    /** @var ToDoOverviewService */
    private $toDoOverviewService;

    public function setUp(): void
    {
        $this->toDoRepository      = $this->prophesize(ToDoRepository::class);
        $this->toDoOverviewService = new ToDoOverviewService($this->toDoRepository->reveal());
    }

    /**
     * @test
     */
    public function list_CallsRepository_ReturnsToDoCollection(): void
    {
        $expectedIdentifier = uniqid('todo:');
        $expectedToDoList   = $this->prophesize(ToDoList::class)->reveal();
        $showToDoOverview   = new OverviewDto($expectedIdentifier);

        $this->toDoRepository->findByUserIdentifier(Argument::which('__toString', $expectedIdentifier))->willReturn($expectedToDoList);

        $actual = $this->toDoOverviewService->list($showToDoOverview);

        self::assertSame($expectedToDoList, $actual);
    }
}
