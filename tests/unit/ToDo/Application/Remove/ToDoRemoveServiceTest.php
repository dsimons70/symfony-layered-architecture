<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Application\Remove;

use App\ToDo\Application\Remove\RemoveDto;
use App\ToDo\Application\Remove\ToDoRemoveService;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use RuntimeException;

/**
 * @covers \App\ToDo\Application\Remove\ToDoRemoveService
 * @covers \App\ToDo\Application\Remove\RemoveDto
 */
class ToDoRemoveServiceTest extends TestCase
{
    /** @var ToDoRepository */
    private $toDoRepository;

    /** @var ToDoRemoveService */
    private $toDoRemoveService;

    /** @var RemoveDto */
    private $removeToDo;

    /** @var ToDo */
    private $toDo;

    public function setUp(): void
    {
        $this->toDoRepository    = $this->prophesize(ToDoRepository::class);
        $this->toDoRemoveService = new ToDoRemoveService($this->toDoRepository->reveal());
        $this->removeToDo        = new RemoveDto('some_identifier');
        $this->toDo              = $this->prophesize(ToDo::class);
    }

    /**
     * @test
     */
    public function remove_RepositoryReturnsFalse_ThrowsException(): void
    {
        $this->expectException(RuntimeException::class);

        $this->toDoRepository->findByIdentifier(Argument::type(ToDoIdentifier::class))->willReturn($this->toDo);
        $this->toDoRepository->remove($this->toDo)->willReturn(false)->shouldBeCalled();

        $this->toDoRemoveService->remove($this->removeToDo);
    }

    /**
     * @test
     */
    public function remove_Success_ReturnsSuccessMessage(): void
    {
        $expectedDate  = ScheduledDate::fromString('2019-10-03');
        $expectedCount = 4;

        $this->toDoRepository->findByIdentifier(Argument::type(ToDoIdentifier::class))->willReturn($this->toDo);
        $this->toDoRepository->remove($this->toDo)->shouldBeCalled();
        $this->toDo->getScheduledDate()->willReturn($expectedDate);
        $this->toDo->count()->willReturn($expectedCount);

        $actual = $this->toDoRemoveService->remove($this->removeToDo);

        self::assertStringContainsString(strval($expectedDate), $actual);
        self::assertStringContainsString(strval($expectedCount), $actual);
    }
}
