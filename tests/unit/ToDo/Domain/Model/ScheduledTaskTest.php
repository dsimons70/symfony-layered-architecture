<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\ScheduledTask
 */
class ScheduledTaskTest extends TestCase
{
    /** @var TaskIdentifier */
    private $expectedIdentifier;

    /** @var ScheduledTime */
    private $expectedTime;

    /** @var ScheduledTask */
    private $actualTask;

    /** @var string */
    private $expectedDescription;

    public function setUp(): void
    {
        $this->expectedIdentifier  = TaskIdentifier::fromString(uniqid('task:'));
        $this->expectedTime        = ScheduledTime::fromString(sprintf('%02s:%02s', rand(0, 23), rand(0, 59)));
        $this->expectedDescription = 'Some text';

        $this->actualTask = ScheduledTask::schedule(
            $this->expectedIdentifier,
            $this->expectedTime,
            $this->expectedDescription
        );
    }

    /**
     * @test
     */
    public function schedule_SetValues_ReturnsSameValues(): void
    {
        self::assertSame($this->expectedIdentifier, $this->actualTask->getTaskIdentifier());
        self::assertSame($this->expectedTime, $this->actualTask->getScheduledTime());
        self::assertSame($this->expectedDescription, $this->actualTask->getDescription());
        self::assertFalse($this->actualTask->isDone());
    }

    /**
     * @test
     */
    public function markAsDone_ChangesStatusToDone(): void
    {
        self::assertFalse($this->actualTask->isDone());

        $this->actualTask->markAsDone();

        self::assertTrue($this->actualTask->isDone());
    }
}
