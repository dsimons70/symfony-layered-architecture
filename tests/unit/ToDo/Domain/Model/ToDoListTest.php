<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\ToDo\Domain\Model\ToDoList;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\ToDoList
 */
class ToDoListTest extends TestCase
{
    /**
     * @test
     */
    public function getIterator_ReturnsSameValues()
    {
        $toDo1 = $this->mockToDo();
        $toDo2 = $this->mockToDo();

        $expectedArray = [
            strval($toDo1->getIdentifier()) => $toDo1,
            strval($toDo2->getIdentifier()) => $toDo2,
        ];

        $toDoList = ToDoList::fromArray($expectedArray);

        $actual = $toDoList->getIterator()->getArrayCopy();

        self::assertSame($expectedArray, $actual);
    }

    /**
     * @test
     */
    public function count_ReturnsValidCount()
    {
        $toDo1 = $this->mockToDo();
        $toDo2 = $this->mockToDo();

        $toDos = [
            strval($toDo1->getIdentifier()) => $toDo1,
            strval($toDo2->getIdentifier()) => $toDo2,
        ];

        $toDoList      = ToDoList::fromArray($toDos);
        $expectedCount = count($toDos);

        $actual = $toDoList->count();

        self::assertSame($expectedCount, $actual);
    }

    private function mockToDo(): ToDo
    {
        $identifier = ToDoIdentifier::fromString(uniqid('todo:'));

        /** @var ToDo $todo */
        $todo = $this->prophesize(ToDo::class);
        $todo->getIdentifier()->willReturn($identifier);

        return $todo->reveal();
    }
}
