<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledDate;
use App\ToDo\Domain\Model\ScheduledTask;
use App\ToDo\Domain\Model\ScheduledTime;
use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDo;
use App\ToDo\Domain\Model\ToDoIdentifier;
use App\User\Domain\Model\UserIdentifier;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @covers \App\ToDo\Domain\Model\ToDo
 * @covers \App\ToDo\Domain\Exception\ToDoRuntimeException
 */
class ToDoTest extends TestCase
{
    /** @var ToDoIdentifier */
    private $toDoIdentifier;

    /** @var string */
    private $userIdentifier;

    /** @var ScheduledDate */
    private $scheduledDate;

    public function setUp(): void
    {
        $this->toDoIdentifier = $this->prophesize(ToDoIdentifier::class)->reveal();
        $this->userIdentifier = $this->prophesize(UserIdentifier::class)->reveal();
        $this->scheduledDate  = $this->prophesize(ScheduledDate::class)->reveal();
    }

    /**
     * @test
     */
    public function scheduleFor_SetsValues_ReturnsSameValues(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);

        self::assertCount(0, $toDo);
        self::assertSame($this->toDoIdentifier, $toDo->getIdentifier());
        self::assertSame($this->userIdentifier, $toDo->getUserIdentifier());
        self::assertSame($this->scheduledDate, $toDo->getScheduledDate());
        self::assertEmpty($toDo->getScheduledTasks());
    }

    /**
     * @test
     */
    public function scheduleTask_MaxNumOfTaskExceeded_ThrowsException(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);

        $this->expectException(ToDoRuntimeException::class);

        $toDo->scheduleTask($this->mockTask('15:00', true));
        $toDo->scheduleTask($this->mockTask('11:00', true));
        $toDo->scheduleTask($this->mockTask('02:45', true));
        $toDo->scheduleTask($this->mockTask('05:45', true));
        $toDo->scheduleTask($this->mockTask('22:45', true));
        $toDo->scheduleTask($this->mockTask('22:55', true));
    }

    /**
     * @test
     */
    public function scheduleTask_TaskWithSameTimeExists_ThrowsException(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);

        $this->expectException(ToDoRuntimeException::class);

        $toDo->scheduleTask($this->mockTask('15:00', true));
        $toDo->scheduleTask($this->mockTask('11:00', true));
        $toDo->scheduleTask($this->mockTask('15:00', true));
    }

    /**
     * @test
     * allTaskDoneWillReturnTrue()
     */
    public function allTasksDone_ReturnsTrue(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);
        $toDo->scheduleTask($this->mockTask('15:00', true));
        $toDo->scheduleTask($this->mockTask('11:00', true));
        $toDo->scheduleTask($this->mockTask('22:45', true));

        $actual = $toDo->allTasksDone();

        self::assertTrue($actual);
    }

    /**
     * @test
     */
    public function allTasksDone_ReturnFalse(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);
        $toDo->scheduleTask($this->mockTask('15:00', true));
        $toDo->scheduleTask($this->mockTask('11:00', false));
        $toDo->scheduleTask($this->mockTask('22:45', true));

        $actual = $toDo->allTasksDone();

        self::assertFalse($actual);
    }

    /**
     * @test
     */
    public function getIterator_ReturnsAllTasks(): void
    {
        $toDo = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);

        $expectedTask  = $this->mockTask('15:00', true);
        $expectedCount = 1;

        $toDo->scheduleTask($expectedTask);

        $actual = $toDo->getIterator();

        self::assertSame($expectedTask, $actual->current());
        self::assertSame($expectedCount, $actual->count());
    }

    /**
     * @test
     */
    public function getUndoneTasks_ReturnsOneTask(): void
    {
        $toDo         = ToDo::scheduleFor($this->userIdentifier, $this->scheduledDate, $this->toDoIdentifier);
        $expectedTask = $this->mockTask('12:55', false);

        $toDo->scheduleTask($this->mockTask('05:00', true));
        $toDo->scheduleTask($this->mockTask('15:20', true));
        $toDo->scheduleTask($expectedTask);
        $toDo->scheduleTask($this->mockTask('20:15', true));

        $actual = $toDo->getUndoneTasks();

        self::assertCount(1, $actual);
        self::assertContainsEquals($expectedTask, $actual);
    }

    private function mockTask(string $time, bool $isDone): ScheduledTask
    {
        $scheduledTime  = ScheduledTime::fromString($time);
        $taskIdentifier = TaskIdentifier::fromString(uniqid('task:'));

        /** @var ScheduledTask|ObjectProphecy $task */
        $task = $this->prophesize(ScheduledTask::class);
        $task->getTaskIdentifier()->willReturn($taskIdentifier);
        $task->getScheduledTime()->willReturn($scheduledTime);
        $task->isDone()->willReturn($isDone);

        return $task->reveal();
    }
}
