<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Model\ToDoIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\ToDoIdentifier
 */
class ToDoIdentifierTest extends TestCase
{
    /**
     * @test
     */
    public function testToString__FromString_ReturnsSameString(): void
    {
        $expectedIdentifier = uniqid('todo:');

        $actual = ToDoIdentifier::fromString($expectedIdentifier);

        self::assertSame($expectedIdentifier, (string) $actual);
        self::assertInstanceOf(ToDoIdentifier::class, $actual);
    }
}
