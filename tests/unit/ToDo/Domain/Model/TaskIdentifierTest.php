<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Model\TaskIdentifier;
use App\ToDo\Domain\Model\ToDoIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\TaskIdentifier
 * @covers \App\Shared\Domain\Model\Identifier
 */
class TaskIdentifierTest extends TestCase
{
    /**
     * @test
     */
    public function toString_FromStringIdentifier_ReturnsSameString(): void
    {
        $expectedIdentifier = uniqid('task:');
        $taskIdentifier     = TaskIdentifier::fromString($expectedIdentifier);

        $actual = (string) $taskIdentifier;

        self::assertSame($expectedIdentifier, $actual);
        self::assertInstanceOf(TaskIdentifier::class, $taskIdentifier);
    }

    /**
     * @test
     */
    public function equals_EqualIdentifier_ReturnsTrue(): void
    {
        $taskIdentifier       = TaskIdentifier::fromString('equals');
        $taskIdentifierEquals = TaskIdentifier::fromString('equals');

        $actual = $taskIdentifier->equals($taskIdentifierEquals);

        self:: assertTrue($actual);
    }

    /**
     * @test
     */
    public function equals_DifferentIdentifier_ReturnsFalse(): void
    {
        $taskIdentifier       = TaskIdentifier::fromString('equals');
        $taskIdentifierEquals = TaskIdentifier::fromString('not_equals');

        $actual = $taskIdentifier->equals($taskIdentifierEquals);

        self:: assertFalse($actual);
    }

    /**
     * @test
     */
    public function equals_ImplementsDifferentInterface_ReturnsFalse(): void
    {
        $taskIdentifier       = TaskIdentifier::fromString('equals');
        $taskIdentifierEquals = ToDoIdentifier::fromString('equals');

        $actual = $taskIdentifier->equals($taskIdentifierEquals);

        self:: assertFalse($actual);
    }
}
