<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledDate;
use DateTime;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\ScheduledDate
 * @covers \App\ToDo\Domain\Exception\ToDoRuntimeException
 */
class ScheduledDateTest extends TestCase
{
    /**
     * @test
     */
    public function toString_ReturnsFormattedString(): void
    {
        $nowDate        = new DateTime('now');
        $expectedString = $nowDate->format("Y-m-d");

        $actual = ScheduledDate::fromString('now');

        self::assertSame($expectedString, $actual->__toString());
    }

    /**
     * @test
     */
    public function fromPhpDateString_InvalidString_ThrowsException(): void
    {
        $this->expectException(ToDoRuntimeException::class);
        ScheduledDate::fromString('i-n-v-a-l-i-d');
    }

    /**
     * @test
     */
    public function toTimestamp_ConvertsToTimestamp(): void
    {
        $nowDate           = new DateTime('now');
        $expectedTimestamp = $nowDate->getTimestamp();

        $actual = ScheduledDate::fromString('now');

        self::assertSame($expectedTimestamp, $actual->toTimestamp());
    }

    /**
     * @test
     */
    public function isInThePast_LaterDate_ReturnsTrue(): void
    {
        $scheduledDate = ScheduledDate::fromString('yesterday');

        self::assertTrue($scheduledDate->isInThePast());

    }

    /**
     * @test
     */
    public function isInThePast_EarlierDate_ReturnsFalse(): void
    {
        $scheduledDate = ScheduledDate::fromString('now');

        self::assertFalse($scheduledDate->isInThePast());
    }
}
