<?php

declare(strict_types=1);

namespace App\Tests\ToDo\Domain\Model;

use App\ToDo\Domain\Exception\ToDoRuntimeException;
use App\ToDo\Domain\Model\ScheduledTime;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\ToDo\Domain\Model\ScheduledTime
 * @covers \App\ToDo\Domain\Exception\ToDoRuntimeException
 */
class ScheduledTimeTest extends TestCase
{
    /**
     * @test
     * @dataProvider provideInvalid
     */
    public function fromTimeString_InvalidString_ThrowException(string $invalid): void
    {
        $this->expectException(ToDoRuntimeException::class);
        ScheduledTime::fromString($invalid);
    }

    /**
     * @test
     */
    public function fromTimeString_ValidString_ReturnsValidValues(): void
    {
        $expectedHour   = 15;
        $expectedMinute = 30;
        $expectedString = $expectedHour . ':' . $expectedMinute;

        $actual = ScheduledTime::fromString($expectedString);

        self::assertSame($expectedHour, $actual->getHour());
        self::assertSame($expectedMinute, $actual->getMinute());
        self::assertSame($expectedString, $actual->__toString());
    }

    /**
     * @test
     */
    public function combinedTimeComparison_EqualTime_ReturnsZero(): void
    {
        $isEqualTime   = ScheduledTime::fromString('15:00');
        $scheduledTime = ScheduledTime::fromString('15:00');

        $actual = $scheduledTime->combinedTimeComparison($isEqualTime);

        self::assertSame(0, $actual);
    }

    /**
     * @test
     */
    public function combinedTimeComparison_EarlierTime_ReturnsMinus1(): void
    {
        $scheduledTime = ScheduledTime::fromString('15:00');
        $isEarlierTime = ScheduledTime::fromString('14:59');

        $actual = $scheduledTime->combinedTimeComparison($isEarlierTime);

        self::assertSame(-1, $actual);
    }

    /**
     * @test
     */
    public function combinedTimeComparison_LaterTimeString_ReturnsPlus1(): void
    {
        $scheduledTime = ScheduledTime::fromString('15:00');
        $isLaterTime   = ScheduledTime::fromString('15:01');

        self::assertSame(1, $scheduledTime->combinedTimeComparison($isLaterTime));
    }

    /**
     * @test
     */
    public function equals_EqualTime_ReturnsTrue()
    {
        $scheduledTime = ScheduledTime::fromString('05:05');
        $isEqualTime   = ScheduledTime::fromString('5:05');

        $actual = $scheduledTime->equals($isEqualTime);

        self::assertTrue($actual);
    }

    /**
     * @test
     */
    public function equals_LaterTime_ReturnsFalse()
    {
        $scheduledTime = ScheduledTime::fromString('05:05');
        $isLaterTime   = ScheduledTime::fromString('5:06');

        $actual = $scheduledTime->equals($isLaterTime);

        self::assertFalse($actual);
    }

    /**
     * @return string[]
     */
    public function provideInvalid(): array
    {
        return [
            ['15/00'],
            ['24:00'],
            ['12:60'],
            ['null'],
            [''],
            ['5:62'],
            ['12.0'],
            ['0:79'],
        ];
    }
}
