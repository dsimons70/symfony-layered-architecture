<?php

declare(strict_types=1);

namespace App\Tests\Shared\Presentation\ViewHelper;

use App\Shared\Domain\Exception\ClientNotification;
use App\Shared\Presentation\ViewHelper\NotificationsViewHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * @covers \App\Shared\Presentation\ViewHelper\NotificationsViewHelper
 */
class NotificationsViewHelperTest extends TestCase
{
    /** @var ConstraintViolationList */
    private $constraintViolationList;

    /** @var ConstraintViolation */
    private $constraintViolation;

    /** @var Constraint */
    private $constraint;

    /** @var NotificationsViewHelper */
    private $notificationsViewHelper;

    public function setUp(): void
    {
        $this->constraint              = $this->prophesize(Constraint::class);
        $this->constraintViolation     = $this->prophesize(ConstraintViolation::class);
        $this->constraintViolationList = new ConstraintViolationList([$this->constraintViolation->reveal()]);

        $this->constraintViolation->getConstraint()->willReturn($this->constraint);
    }

    /**
     * @test
     */
    public function fromNotificationException_SetValues_ReturnSameValues()
    {
        $expectedKey     = 'ScheduledDate';
        $expectedMessage = 'Some date error';
        $expectedArray   = [$expectedKey => $expectedMessage];

        /** @var ClientNotification $notificationException */
        $notificationException = $this->prophesize(ClientNotification::class);
        $notificationException->getUserNotification()->willReturn($expectedMessage);
        $notificationException->getOrigin()->willReturn($expectedKey);

        $this->notificationsViewHelper = NotificationsViewHelper::fromNotificationException($notificationException->reveal());

        self::assertSame($expectedMessage, $this->notificationsViewHelper->getViolationMessage($expectedKey));
        self::assertSame($expectedArray, $this->notificationsViewHelper->getNotifications());
        self::assertEquals($expectedArray, $this->notificationsViewHelper->getIterator()->getArrayCopy());
    }

    /**
     * @test
     * @dataProvider provide
     */
    public function hasViolation_ExpectedViolationExists_ReturnsTrue(
        string $propertyPath,
        string $alias,
        string $identifier,
        string $expectedKey,
        string $expectedMessage
    ) {
        $this->constraintViolation->getPropertyPath()->willReturn($propertyPath);
        $this->constraintViolation->getMessage()->willReturn($expectedMessage);
        $this->constraint->payload     = $this->buildPayload($alias, $identifier);
        $this->notificationsViewHelper = NotificationsViewHelper::fromConstraintViolationList($this->constraintViolationList);

        $actual        = $this->notificationsViewHelper->hasViolation($expectedKey);
        $actualMessage = $this->notificationsViewHelper->getViolationMessage($expectedKey);

        self::assertTrue($actual);
        self::assertSame($expectedMessage, $actualMessage);
    }

    /**
     * @test
     */
    public function hasViolation_ExpectedKeyNotExists_ReturnsFalse()
    {
        $this->constraintViolation->getPropertyPath()->willReturn('scheduledTasks[6a4f038a-b020-41f3-96a5-9d714ab53d72].time');
        $this->constraintViolation->getMessage()->willReturn('some_error');
        $this->notificationsViewHelper = NotificationsViewHelper::fromConstraintViolationList($this->constraintViolationList);
        $expectedMessage               = '';

        $actual        = $this->notificationsViewHelper->hasViolation('key_does_not_exists');
        $actualMessage = $this->notificationsViewHelper->getViolationMessage('key_does_not_exists');

        self::assertFalse($actual);
        self::assertSame($expectedMessage, $actualMessage);
    }

    public function provide(): array
    {
        return [
            "test_replacement_for_edit_and_schedule_dto"     => [
                "propertyPath" => 'editTasks[6a4f038a-b020-41f3-96a5-9d714ab53d72].scheduledTime',
                "alias"        => 'ScheduledTime',
                "prefix"       => '/\[(?<prefix>[^]]+)\]/',
                "expected"     => '6a4f038a-b020-41f3-96a5-9d714ab53d72.ScheduledTime',
                "message"      => 'Task time not defined',
            ],
            "test_replacement_with_renamed_property"         => [
                "propertyPath" => 'renamedEditTasks[6a4f038a-b020-41f3-96a5-9d714ab53d72].scheduledTime',
                "alias"        => 'time',
                "prefix"       => '/\[(?<prefix>[^]]+)\]/',
                "expected"     => '6a4f038a-b020-41f3-96a5-9d714ab53d72.time',
                "message"      => 'Task time not defined',
            ],
            "test_incomplete_payload_will_use_property_path" => [
                "propertyPath" => 'scheduledTasks[6a4f038a-b020-41f3-96a5-9d714ab53d72].scheduledTime',
                "alias"        => '',
                "prefix"       => '',
                "expected"     => 'scheduledTasks[6a4f038a-b020-41f3-96a5-9d714ab53d72].scheduledTime',
                "message"      => 'ScheduledTime is not defined',
            ],
            "test_unchanged"                                 => [
                "propertyPath" => 'scheduledDate',
                "alias"        => '',
                "prefix"       => '',
                "expected"     => 'scheduledDate',
                "message"      => 'Date is not defined',
            ],
        ];
    }

    private function buildPayload(string $alias, string $identifier): array
    {
        return [
            "alias"  => $alias,
            "prefix" => $identifier,
        ];
    }
}
