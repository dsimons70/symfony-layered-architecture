<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Persistence;

use App\Shared\Infrastructure\Persistence\Config\PersistenceConfig;
use App\Shared\Infrastructure\Persistence\MySql\MySqlPersistenceType;
use App\Shared\Infrastructure\Persistence\PersistenceFactory;
use App\Shared\Infrastructure\Persistence\PersistenceIdentification;
use App\Shared\Infrastructure\Persistence\PersistenceType;
use App\Shared\Infrastructure\Persistence\Redis\RedisPersistenceType;
use App\ToDo\Domain\Model\ToDoRepository;
use App\User\Domain\Model\UserRepository;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Shared\Infrastructure\Persistence\PersistenceFactory
 */
class PersistenceFactoryTest extends TestCase
{
    /** @var PersistenceConfig */
    private $persistenceConfig;

    public function setUp(): void
    {
        $this->persistenceConfig = $this->prophesize(PersistenceConfig::class);
    }

    /**
     * @test
     */
    public function isPersistenceTypRegistered_IdentifierFound_ReturnsTrue(): void
    {
        $expectedType       = new RedisPersistenceType();
        $repositories[]     = $this->mockRepository($expectedType, UserRepository::class);
        $persistenceFactory = $this->createPersistenceFactory($repositories);

        $actual = $persistenceFactory->isPersistenceTypRegistered($expectedType->getIdentifier());

        self::assertTrue($actual);
    }

    /**
     * @test
     */
    public function isPersistenceTypRegistered_IdentifierNotFound_ReturnsFalse(): void
    {
        $expectedType       = new MySqlPersistenceType();
        $repositories[]     = $this->mockRepository($expectedType, UserRepository::class);
        $persistenceFactory = $this->createPersistenceFactory($repositories);

        $actual = $persistenceFactory->isPersistenceTypRegistered('not_registered');

        self::assertFalse($actual);
    }

    /**
     * @test
     */
    public function getRepository_InterfaceIsRegistered_ReturnsMatchingRepository(): void
    {
        $configuredType     = new MySqlPersistenceType();
        $repositoryType     = new MySqlPersistenceType();
        $expectedRepository = $this->mockRepository($repositoryType, UserRepository::class);

        $persistenceFactory = $this->createPersistenceFactory(
            [$expectedRepository],
            $configuredType->getIdentifier()
        );

        $actual = $persistenceFactory->getRepository(UserRepository::class);

        self::assertSame($expectedRepository, $actual);
    }

    /**
     * @test
     */
    public function getRepository_PersistenceTypeNotRegistered_ThrowsException(): void
    {
        $configuredType = new MySqlPersistenceType();
        $repositoryType = new RedisPersistenceType();
        $repositories[] = $this->mockRepository($repositoryType, UserRepository::class);

        $persistenceFactory = $this->createPersistenceFactory(
            $repositories,
            $configuredType->getIdentifier()
        );

        $this->expectException(Exception::class);

        $persistenceFactory->getRepository(UserRepository::class);
    }

    /**
     * @test
     */
    public function getRepository_InterfaceNotRegistered_ThrowsException(): void
    {
        $configuredType = new RedisPersistenceType();
        $repositoryType = new RedisPersistenceType();
        $repositories[] = $this->mockRepository($repositoryType, UserRepository::class);

        $persistenceFactory = $this->createPersistenceFactory(
            $repositories,
            $configuredType->getIdentifier()
        );

        $this->expectException(Exception::class);

        $persistenceFactory->getRepository('not_registered_interface');
    }

    /**
     * @test
     */
    public function getAllPersistenceTypes_ReturnsAllTypes(): void
    {
        $expectedTypes['type_mysql'] = new MySqlPersistenceType();
        $expectedTypes['type_redis'] = new RedisPersistenceType();

        $repositories[] = $this->mockRepository($expectedTypes['type_mysql'], UserRepository::class);
        $repositories[] = $this->mockRepository($expectedTypes['type_redis'], UserRepository::class);

        $persistenceFactory = $this->createPersistenceFactory(
            $repositories,
            'some_identifier'
        );

        $actual = $persistenceFactory->getAllPersistenceTypes();

        self::assertSame($expectedTypes, $actual);
    }

    /**
     * @test
     */
    public function addRepository_InterfaceAlreadyRegistered_ThrowsException()
    {
        $configuredType = new MySqlPersistenceType();
        $repositoryType = new RedisPersistenceType();
        $repositories[] = $this->mockRepository($repositoryType, UserRepository::class);
        $repositories[] = $this->mockRepository($repositoryType, UserRepository::class);

        $this->expectException(Exception::class);

        $this->createPersistenceFactory(
            $repositories,
            $configuredType->getIdentifier()
        );
    }

    /**
     * @test
     */
    public function addRepository_InterfaceMismatch_ThrowsException()
    {
        $repositoryWithInterfaceMismatch = $this->prophesize(PersistenceIdentification::class);
        $repositoryWithInterfaceMismatch->willImplement(UserRepository::class);
        $repositoryWithInterfaceMismatch->getPersistenceType()->willReturn(new MySqlPersistenceType);
        $repositoryWithInterfaceMismatch->getImplementedRepositoryInterface()->willReturn(ToDoRepository::class);

        $this->expectException(Exception::class);

        $this->createPersistenceFactory(
            [$repositoryWithInterfaceMismatch->reveal()],
            'some_identifier'
        );
    }

    /**
     * Creates a PersistenceFactory
     */
    private function createPersistenceFactory(
        iterable $repositories,
        string $configuredType = null
    ): PersistenceFactory {
        if ($configuredType !== null) {
            $this->persistenceConfig->getConfiguredType()->willReturn($configuredType);
        }
        return new PersistenceFactory($repositories, $this->persistenceConfig->reveal());
    }

    /**
     * Create Mock for DomainRepositories
     */
    private function mockRepository(PersistenceType $type, string $repositoryInterface): PersistenceIdentification
    {
        /** @var PersistenceIdentification $repo */
        $repo = $this->prophesize(PersistenceIdentification::class);
        $repo->willImplement($repositoryInterface);
        $repo->getPersistenceType()->willReturn($type);
        $repo->getImplementedRepositoryInterface()->willReturn($repositoryInterface);

        return $repo->reveal();
    }
}
