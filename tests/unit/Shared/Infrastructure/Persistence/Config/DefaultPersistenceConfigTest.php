<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Persistence\Config;

use App\Shared\Infrastructure\Persistence\Config\DefaultPersistenceConfig;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DefaultPersistenceConfigTest extends TestCase
{
    /** @var ParameterBagInterface */
    private $parameterBag;

    /** @var DefaultPersistenceConfig */
    private $persistenceConfig;

    public function setUp(): void
    {
        $this->parameterBag      = $this->prophesize(ParameterBagInterface::class);
        $this->persistenceConfig = new DefaultPersistenceConfig($this->parameterBag->reveal());
    }

    /**
     * @test
     */
    public function getConfiguredType_ReturnsConfiguredParameter(): void
    {
        $expectedName      = 'app.persistence.default';
        $expectedParameter = 'type_mysql';

        $this->parameterBag->get($expectedName)->willReturn($expectedParameter);

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedParameter, $actual);
    }
}
