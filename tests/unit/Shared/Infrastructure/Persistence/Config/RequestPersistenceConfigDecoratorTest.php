<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Persistence\Config;

use App\Shared\Infrastructure\Persistence\Config\PersistenceConfig;
use App\Shared\Infrastructure\Persistence\Config\RequestPersistenceConfigDecorator;
use App\Shared\Infrastructure\Persistence\Config\SessionPersistenceConfigDecorator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @covers \App\Shared\Infrastructure\Persistence\Config\RequestPersistenceConfigDecorator
 */
class RequestPersistenceConfigDecoratorTest extends TestCase
{
    /** @var RequestStack */
    private $requestStack;

    /** @var Request */
    private $request;

    /** @var PersistenceConfig */
    private $decoratedPersistenceConfig;

    /** @var SessionPersistenceConfigDecorator */
    private $persistenceConfig;

    public function setUp(): void
    {
        $this->decoratedPersistenceConfig = $this->prophesize(SessionPersistenceConfigDecorator::class);
        $this->requestStack               = $this->prophesize(RequestStack::class);
        $this->request                    = $this->prophesize(Request::class);

        $this->persistenceConfig = new RequestPersistenceConfigDecorator(
            $this->decoratedPersistenceConfig->reveal(),
            $this->requestStack->reveal()
        );

        $this->requestStack->getCurrentRequest()->willReturn($this->request->reveal());
    }

    /**
     * @test
     */
    public function getConfiguredType_RouteMatches_ReturnsValueFromRequest()
    {
        $expectedValue = 'type_mysql';

        $this->request->get('_route')->willReturn('login')->shouldBeCalled();
        $this->request->get('persistenceType')->WillReturn($expectedValue)->shouldBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->shouldNotBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedValue, $actual);
    }

    /**
     * @test
     */
    public function getConfiguredType_RouteMatchesButRequestReturnsNull_ReturnsValueFromDecoratedClass()
    {
        $expectedValue = 'type_mysql';

        $this->request->get('_route')->willReturn('login')->shouldBeCalled();
        $this->request->get('persistenceType')->willReturn(null)->shouldBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->willReturn($expectedValue)->shouldBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedValue, $actual);
    }

    /**
     * @test
     */
    public function getConfiguredType_RouteDoesNotMatch_ReturnsValueFromDecoratedClass()
    {
        $expectedValue = 'type_mysql';

        $this->request->get('_route')->willReturn('does/not/exist')->shouldBeCalled();
        $this->request->get('persistenceType')->shouldNotBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->willReturn($expectedValue)->shouldBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedValue, $actual);
    }

    /**
     * @test
     */
    public function getConfiguredType_RequestStackReturnsNull_ReturnsValueFromDecoratedClass()
    {
        $expectedValue = 'type_mysql';

        $this->request->get('_route')->willReturn('does/not/exist')->shouldBeCalled();
        $this->request->get('persistenceType')->shouldNotBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->willReturn($expectedValue)->shouldBeCalled();
        $this->requestStack->getCurrentRequest()->willReturn(null)->shouldBeCalled();
        $this->request->get('_route')->shouldNotBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedValue, $actual);
    }
}
