<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Persistence\Config;

use App\Shared\Infrastructure\Persistence\Config\DefaultPersistenceConfig;
use App\Shared\Infrastructure\Persistence\Config\PersistenceConfig;
use App\Shared\Infrastructure\Persistence\Config\SessionPersistenceConfigDecorator;
use App\User\Infrastructure\Session\SessionRegistry;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * @covers \App\Shared\Infrastructure\Persistence\Config\SessionPersistenceConfigDecorator
 * @covers \App\Shared\Infrastructure\Persistence\Config\PersistenceConfigDecorator
 */
class SessionPersistenceConfigDecoratorTest extends TestCase
{
    /** @var SessionRegistry */
    private $sessionRegistry;

    /** @var PersistenceConfig */
    private $decoratedPersistenceConfig;

    /** @var SessionPersistenceConfigDecorator */
    private $persistenceConfig;

    public function setUp(): void
    {
        $this->decoratedPersistenceConfig = $this->prophesize(DefaultPersistenceConfig::class);
        $this->sessionRegistry            = $this->prophesize(SessionRegistry::class);

        $this->persistenceConfig = new SessionPersistenceConfigDecorator(
            $this->decoratedPersistenceConfig->reveal(),
            $this->sessionRegistry->reveal()
        );
    }

    /**
     * @test
     */
    public function getConfiguredType_SessionReturnsNull_ReturnsValueFromDecoratedClass(): void
    {
        $expectedType = 'type_redis';

        $this->sessionRegistry->getPersistenceTypeIdentifier()->willReturn(null)->shouldBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->willReturn($expectedType)->shouldBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedType, $actual);
    }

    /**
     * @test
     */
    public function getConfiguredType_SessionValueNotNull_ReturnsValueFromSession(): void
    {
        $expectedType = 'type_mysql';

        $this->sessionRegistry->getPersistenceTypeIdentifier()->willReturn($expectedType)->shouldBeCalled();
        $this->decoratedPersistenceConfig->getConfiguredType()->willReturn(Argument::any())->shouldNotBeCalled();

        $actual = $this->persistenceConfig->getConfiguredType();

        self::assertSame($expectedType, $actual);
    }
}
